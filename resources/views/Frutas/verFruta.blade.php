@extends('admin/layout/admin')
@section('content')
<!-- contenido de la pagina web -->

<div class="container">
  <div class="row">
 
    @foreach ($frutas as $fruta)
   
      
    @php $urlImagen= 'images/'. $fruta->imagen; @endphp


    <div class="col-md-3 my-2">
       <div class="card">
        <img src="{{ asset($urlImagen)}}" class="card-img-top" alt="{{ asset($urlImagen)}}">
          <div class="card-body">

            <h5><th>{{ $fruta->nom_fruta}}</th></h5>
            <h5 class="card-title">Descripcion: </h5>
            <p><th>{{ $fruta->descripcion_f}}</th></p>

            <a href="/admin/ConsultarFruta/{{$fruta->Id_fruta}}" class="btn btn-block btn-primary">ver</a>
           
          
          </div>
      </div>
    </div> 

    @endforeach
  </div>
</div> 
@endsection
@section('js')
<!-- archivos js dependientes de la vista -->
@endsection