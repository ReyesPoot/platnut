<?php

namespace App\Http\Controllers;

use App\Padecidad;
use Illuminate\Http\Request; 

use App\Http\Controllers\Controller;

class PadecidadController extends Controller
{

    public function agregarPadecidad()
    {
        return view ('Padecidades/crearPadecidad');
    }
     public function store(Request $request)
    {

        $Guardar= new padecidad;

        $Guardar->nom_padecimiento= $request->padecidad;
        $Guardar->descripcion= $request->descripcion;
   
        //dd($Guardar);
        $Guardar->save();
        return redirect('/admin/verPadecidad'); 
    }


    public function verPadecidad()
    {
        $padecidades= Padecidad::get();
        return view ('Padecidades/verPadecidad', compact('padecidades'));
    }

    public function EditarPadecidad($id)
    {
    
        $padecidades= Padecidad::where('Id_padecidad',$id)->first();
        return view ('Padecidades/actualizarPadecidad', compact('padecidades'));

    }
    public function updatePadecidad(Request $request)
    {   //dd($_POST);
        
        $padecidades= Padecidad::where('Id_padecidad',$request->id)->first();
        $padecidades->nom_padecimiento=$request->padecidad;
        $padecidades->descripcion=$request->descripcion;
        $padecidades->save();
        
        return redirect('/admin/verPadecidad'); 

    }
    public function EliminarPadecidad($id)
    {
        //dd($id);
        $padecidades= Padecidad::where('Id_padecidad',$id)->first();
         return view ('Padecidades/EliminarPadecidad', compact('padecidades'));

    } 
    
    public function destroy(Request $request)
    {   
        
        $padecidades= Padecidad::where('Id_padecidad',$request->id)->first();
        $padecidades->delete();
        return redirect('/admin/verPadecidad');
        
    }

}
