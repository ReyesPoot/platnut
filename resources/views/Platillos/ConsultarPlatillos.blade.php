@extends('admin/layout/admin')
@section('content')

    @php $urlImagen= 'images/'. $platillo->imagen; @endphp
    
<div class="container">
  <div class="card mb-3">
    <div class="row ">
      <div class="col-md-4">
        <img src="{{ asset($urlImagen)}}" class="img-fluid" alt="{{ asset($urlImagen)}}">
      </div>
      <div class="col-md-8">
        <div class="card-body">
          <h5 class="card-title">PLATILLO</h5>
            <p class="card-text">{{$platillo->nom_platillo}}</p>
          <h5 class="card-title">DESCRIPCION</h5>
          <p class="card-text">{{$platillo->descripcion}}</p>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-5">
        <div class="card-body">
          <h5 class="card-title">BENEFICIOS</h5>
            <p class="card-text">{{$platillo->beneficios}}</p>
        </div>
      </div>
      <div class="col-md-7">
        <div class="card-body">
          <h5 class="card-title">VALOR NUTRICIONAL</h5>
          <p>
            <a class="btn btn-primary" data-toggle="collapse" href="#Informacion" role="button" aria-expanded="false" aria-controls="collapseExample">
             Agregar
            </a>
          </p>
              <div class="collapse" id="Informacion">
                <div class="card card-body">
                   <form action="/admin/agregarInfoPlatillo" method="POST">

                        {{csrf_field()}}
                          <div class="row no-gutters">
                            <input type="text" value="{{$platillo->Id_platillo}}" name="id" hidden>
                            <div class="col">
                                <label for="agregar">Informacion</label>
                                <input type="text" name="informacion" class= "form-control placeholder" placeholder="nombre..">
                            </div>
                            <div class="col">
                              <label for="agregar">Cantidad</label>
                              <input type="text" name="cantidad" class= "form-control placeholder" placeholder="Cantidad..">
                            </div>        
                            <div class="col">
                              <label for="agregar">Unida de medida</label>
                              <input type="text" name="unidad" class= "form-control placeholder" placeholder="Unidad..">
                            </div>  
                            <div class="form-group">
                              <input type="submit" value="Guardar" class= " btn btn-block btn-primary bnt-sm">
                            </div>   
                        </div>
                    </form>
                </div>
              </div>
            <table class="table table-bordered">
              <thead class="thead-dark">
                <tr>
                  <th>Informacion</th>
                  <th>Cantidad</th>
                  <th>Unidad</th>
                </tr>
                </thead>
                <tbody>
                  @foreach($platillo->informacion as $informacion)
                  <tr>
                    <td>{{$informacion->informacion}}</td>
                    <td>{{$informacion->cantidad}}</td>
                    <td>{{$informacion->unidad_medida}}</td>
                      
                  </tr>
                  @endforeach 
                </tbody>
            </table>
        </div>
      </div>
    </div>
    <div class="row">
       <div class="col-md-6">
           <div class="card-body">
          <h5 class="card-title">INGREDIENTES: </h5>
        <p>
        <a class="btn btn-primary" data-toggle="collapse" href="#ingrediente" role="button" aria-expanded="false" aria-controls="collapseExample">
         Agregar
        </a>
        
        </p>
          <div class="collapse" id="ingrediente">
            <div class="card card-body">
               <form action="/admin/agregarIngrediente" method="POST">

                    {{csrf_field()}}
                      <div class="row no-gutters">
                                      
                         <input type="text" value="{{$platillo->Id_platillo}}" name="id" hidden>
                        <div class="col">
                            <label for="agregar">Ingrediente</label>
                            <input type="text" name="ingrediente" class= "form-control placeholder" placeholder="nombre..">
                        </div>
                        <div class="col">
                          <label for="agregar">Cantidad</label>
                          <input type="text" name="cantidad" class= "form-control placeholder" placeholder="Cantidad..">
                        </div>        
                        <div class="col">
                          <label for="agregar">Unida de medida</label>
                          <input type="text" name="unidad" class= "form-control placeholder" placeholder="Unidad..">
                        </div>  
                        <div class="form-group">
                          <input type="submit" value="Agregar" class= " btn btn-block btn-primary bnt-sm">

                        </div>   
                    </div>
                </form>
            </div>
          </div>
           
                      @foreach($platillo->ingredientes as $ingredientes)
                     <p>{{$ingredientes->cantidad}} {{$ingredientes->unidad_medida}} de <th>{{$ingredientes->ingrediente}}</th></p>
                      @endforeach
        </div>
      </div>
      <div class="col-md-6">
        <div class="card-body">
          <h5 class="card-title">PREPARACION</h5>
          <p>
        <a class="btn btn-primary" data-toggle="collapse" href="#preparacion" role="button" aria-expanded="false" aria-controls="collapseExample">
         Agregar
        </a>
        </p>
          <div class="collapse" id="preparacion">
            <div class="card card-body">
               <form action="/admin/agregarInstrucciones" method="POST">

                    {{csrf_field()}}
                      <div class="row no-gutters">
                                      
                         <input type="text" value="{{$platillo->Id_platillo}}" name="id" hidden>
                        <div class="col">
                            <label for="agregar">Instrucciones</label>
                            <input type="text" name="instrucciones" class= "form-control placeholder" placeholder="Instruciones..">
                        </div> 
                        <div class="form-group">
                          <input type="submit" value="Agregar" class= " btn btn-block btn-primary bnt-sm">

                        </div>  
                    </div>
                </form>
            </div>
          </div>
          <?php $c=1;  ?>
           @foreach($platillo->instrucciones as $instrucciones)
           <p> paso {{$c++}}:{{$instrucciones->instrucciones}} </p>
           @endforeach
        </div>
      </div>
    </div>
    <center>
        <div class="card-body">
          <a href="/admin/verPlatillo" class="btn btn-outline-info">Regresar</a>
          <a href="/admin/ConsultarActualizar/{{$platillo->Id_platillo}}" class="btn btn-success">Actualizar</a>
          <a href="/admin/ConsultarEliminar/{{$platillo->Id_platillo}}" class="btn btn-danger">Eliminar</a>
        </div>  
   </center> 
  </div>
</div>
                                  
      
@endsection

@section('js')

 @endsection