<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fruta extends Model 
{

	protected $fillable= ['imagen','Id_fruta','nom_fruta','nom_cientifico','descripcion_f','beneficios_f','receta_f','fecha_created','fecha_updated'];

	protected $primaryKey= 'Id_fruta';
	protected $table= 'fruta';
	const CREATED_AT ='fecha_created';
	const UPDATED_AT ='fecha_updated';


	public function informacion(){
		return $this->belongsToMany('App\Informacion', 'info_fruta', 'Id_fruta','Id_informacion');
	}
	public function platillo(){
		return $this->belongsToMany('App\platillo', 'platillo_fruta', 'Id_platillo','Id_fruta');
	}
}

 ?>