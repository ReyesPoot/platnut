<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class info_fruta extends Model 
{

	protected $fillable= ['Id_info_fruta','Id_informacion','Id_fruta','fecha_created','fecha_updated'];

	//protected $primaryKey= 'Id_analisis';
	protected $table= 'info_fruta';
	const CREATED_AT ='fecha_created';
	const UPDATED_AT ='fecha_updated';

	//Relaciones
	
	public function informacion(){
		return $this->belongsToMany('App/Informacion','Id_informacion','Id_fruta');

	}

}

 ?>