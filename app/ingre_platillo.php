<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ingre_platillo extends Model 
{

protected $fillable= ['Id_ingre_platillo','Id_platillo','Id_ingrediente'];

protected $primaryKey= 'Id_ingre_platillo';
protected $table= 'ingre_platillo';

public function ingredientes(){
		return $this->belongsToMany('App/Ingrediente','Id_platillo','Id_ingrediente');

	}



}


 ?>