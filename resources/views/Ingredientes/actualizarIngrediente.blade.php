@extends('admin/layout/admin')
@section('content')
<!-- contenido de la pagina web -->
<div class="container" >
    <div class="cold-md-6">
        <h2>Actualizar Ingrediente del platillo </h2>
            <form action="/admin/editarIngrediente" method="POST">

                {{csrf_field()}}
                        <input type="text" value="{{$ingredientes->Id_ingrediente}}" name="id" hidden>
                        <input type="text" value="{{$id_platillo}}" name="id_platillo" hidden>
                        <div class="form-group">
                            <label for="agregar verduras">Ingrediente</label>
                            <input type="text" value="{{$ingredientes->ingrediente}}" name="ingrediente" class= "form-control placeholder" >
                        </div>
                        <div class="form-group">
                            <label for="agregar">Cantidad</label>
                            <input type="text" value="{{$ingredientes->cantidad}}" name="cantidad" class= "form-control placeholder" >
                        </div>
                        <div class="form-group">
                            <label for="agregar verduras">Unidad de medida</label>
                            <input type="text" value="{{$ingredientes->unidad_medida}}" name="unidad_medida" class= "form-control placeholder" >
                        </div>
            <center>
                
                <div class="form-group">
                    <button type="submit" class= "btn btn-success">
                        Actualizar
                    </button>
                </div>
            </center>              
            
        </form> 
    </div>    
</div> 
       
@endsection
@section('js')

@endsection