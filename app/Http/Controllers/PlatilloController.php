<?php

namespace App\Http\Controllers;

use App\Platillo;
use App\Informacion;
use App\Ingrediente;
use App\Procedimiento;
use App\Fruta;
use App\Verdura;
use App\instrucciones;

use Illuminate\Http\Request; 

use App\Http\Controllers\Controller;

class PlatilloController extends Controller
{

    //-------------------administrador de platillos--------------------------
  
    
    public function agregarPlatillo()
    {
        $title="crearPlatillo";
        return view ('Platillos/crearPlatillo',compact('title'));
    }

    //consulta de ver platillos
    public function Consultar($id)
    {
       $title="crearPlatillo";
        //consultando informacion nutrimental de los platillos
        $platillo=Platillo::where('Id_platillo',$id)->first();
        $Consulta =Platillo::find($id)->informacion()->get();
        $ingrediente=Ingrediente::where('Id_ingrediente',$id)->first();
        $Consultar=Platillo::find($id)->ingredientes()->get();
        $instrucciones= instrucciones::where('Id_instrucciones',$id)->first();
        $verinst= Platillo::find($id)->instrucciones()->get();
    

        return view ('Platillos/ConsultarPlatillos',compact('title','Consulta','platillo','Consultar','ingrediente','verinst'));
       
    } 

    
    public function storeFruta(Request $request)
    {

        $Guardar= new platillo;
        $Guardar->imagen= $request->imagen;
        $Guardar->nom_platillo= $request->nom_platillo;
        $Guardar->descripcion= $request->descripcion;
        $Guardar->beneficios= $request->beneficios;
       
        //dd($Guardar);
        $Guardar->save();

        $id_info=$Guardar->Id_platillo;//obteniendo el id de la informacion
                                        //agregando informacion nutrimental de los prodcutos en la tabla pivote
        $info=Fruta::find($request->id);
       
        $info->Platillo()->attach($id_info);
      
        return redirect('/admin/verPlatillo'); 
    }
    public function storeVerdura(Request $request)
    {

        $Guardar= new platillo;
        $Guardar->imagen= $request->imagen;
        $Guardar->nom_platillo= $request->nom_platillo;
        $Guardar->descripcion= $request->descripcion;
        $Guardar->beneficios= $request->beneficios;
       
        //dd($Guardar);
        $Guardar->save();

        $id_info=$Guardar->Id_platillo;//obteniendo el id de la informacion
                                        //agregando informacion nutrimental de los prodcutos en la tabla pivote
        $info=Verdura::find($request->id);
       
        $info->Platillo()->attach($id_info);
      
        return redirect('/admin/verPlatillo'); 
    }
    public function verPlatillo()

    {
        $title="crearPlatillo";
        $platillos= Platillo::get();

        return view ('Platillos/verPlatillo', compact('platillos','title'));
    }

     public function EditarConsultar($id)
    {
        $title="crearPlatillo";
        $platillo= Platillo::where('Id_platillo',$id)->first();
        

        return view ('Platillos/ConsultarActualizar', compact('title','platillo'));

    }

     public function updatePlatillo(Request $request)
    {   
        //dd($_POST);
        
        $platillo= Platillo::where('Id_platillo',$request->id)->first();
        $platillo->imagen= $request->imagen;
        $platillo->nom_platillo=$request->nom_platillo;
        $platillo->descripcion=$request->descripcion;
        $platillo->beneficios=$request->beneficios;
        $platillo->save();

        return redirect('/admin/verPlatillo'); 

    }
    public function EliminarConsultar($id)
    {
        $title="crearPlatillo";
        //dd($id);
        $ingrediente=Ingrediente::where('Id_ingrediente',$id)->first();
        $consultar= Platillo::find($id)->ingredientes()->get();
        $informacion=Informacion::where('Id_informacion',$id)->first();
        $platillo= Platillo::where('Id_platillo',$id)->first();
         return view ('Platillos/ConsultarEliminar', compact('title','platillo','consultar','ingrediente','informacion'));

    } 
     public function destroy(Request $request)
    {   

        
        $platillos= Platillo::where('Id_platillo',$request->id)->first();
        $platillos->delete();
        return redirect('/admin/verPlatillo');
        
    }




}