<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class instrucciones extends Model 
{

	protected $fillable= ['Id_instrucciones','instrucciones'];

	protected $primaryKey= 'Id_instrucciones';
	protected $table= 'instrucciones';
	const CREATED_AT ='fecha_created';
	const UPDATED_AT ='fecha_updated';

	public function platillo_instru(){
		return $this->belongsToMany('App\platillo', 'instrucciones_platillo', 'Id_platillo','Id_instrucciones');
	}
}

 ?>