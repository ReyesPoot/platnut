@extends('admin/layout/admin')
@section('content')
<!-- contenido de la pagina web -->

<div class="container">
  <div class="row">
    @foreach ($plantas as $planta)
    @php $urlImagen= 'images/'. $planta->imagen; @endphp
      
    <div class="col-md-3 my-2">
       <div class="card">
        <img src="{{ asset($urlImagen)}}" class="card-img-top" alt="{{ asset($urlImagen)}}">
          <div class="card-body">
            <h5 class="card-title">{{$planta->nom_planta}}</h5>
            <h5 class="card-title">Descripcion</h5>
            <p><th>{{ $planta->descripcion_p }}</th></p>

            <a href="/admin/ConsultarPlanta/{{$planta->Id_planta}}" class="btn btn-block btn-primary">ver</a>
          
           
            
          </div>
      </div>
    </div> 

    @endforeach
  </div>
</div> 
     
       
@endsection
@section('js')
<!-- archivos js dependientes de la vista -->
@endsection