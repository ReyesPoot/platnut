<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'Id_admin', 'nom_usuario', 'password','fecha_created','fecha_updated','remember_token'];

    protected $primaryKey= 'Id_admin';
    protected $table= 'administrador';
    const CREATED_AT ='fecha_created';
    const UPDATED_AT ='fecha_updated';

  
}
