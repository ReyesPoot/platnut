<!DOCTYPE html>
<html lang="es">
  <head>
    <title>ADMIN | PLATNUT</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/admin/main.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/admin.min.css')}}">
    <link media = "all" rel="stylesheet" type="text/css" href="{{ asset('css/admin/fileinput.css')}}">
    <link media = "all" rel="stylesheet" type="text/css" href="{{ asset('css/admin/fileinput-rtl.css')}}">
    <link media = "all" rel="stylesheet" type="text/css" href="{{ asset('css/admin/theme.css')}}">
    <link rel="stylesheet" href="{{ asset('css/lightbox.min.css')}}">
    <link media = "all" rel="stylesheet" type="text/css" href="{{ asset('css/admin/bootstrap-tagsinput.css')}}">
    <link media = "all" rel="stylesheet" type="text/css" href="{{ asset('css/admin/tags.css')}}">
    <link media = "all" rel="stylesheet" type="text/css" href="{{ asset('css/admin/skin.css')}}">
    <link rel="shortcut icon" href="{{ asset('logo.ico')}}">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.css">
  </head>
  <body class="app sidebar-mini rtl">

    <header class="app-header">
      <a class="app-header__logo d-none d-sm-inline-block" href="">
        Panel de control
      </a>
      <a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar">
        <i class="fas fa-bars"></i>
      </a>
      <h5 class="mt-3 titleadmin opensans d-none d-sm-block adaptar">PLATNUT - MÀ'ALO'OB JA'ANÀL</h5>
      <h5 class="mt-3 titleadmin opensans d-block d-sm-none">SELVA BONITA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h5>
      <!-- Navbar Right Menu-->
      <ul class="app-nav">
  
        <!--Notification Menu-->
        <li class="dropdown">
          <a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Show notifications">
            <i class="fas fa-bell"></i>
          </a>
          
          <ul class="app-notification dropdown-menu dropdown-menu-right">
            <li class="app-notification__title">Tienes 0 notificacion(es)</li>
            <div class="app-notification__content">
                <li>
                  <a class="app-notification__item" href="">
                    <span class="app-notification__icon">
                      <span class="fa-stack fa-lg">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-envelope fa-stack-1x fa-inverse"></i>
                      </span>
                    </span>
                    <div>
                      <p class="app-notification__message">Hay un nuevo </p>
                      <p class="app-notification__meta">lorem lorem lorem</p>
                    </div>
                  </a>
                </li>
            </div>
            <li class="app-notification__footer">
              <a href="">Ver todas las notificaciones</a>
            </li>
          </ul>
        </li>

        <!-- User Menu-->
        <li class="dropdown">

          <a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu">
            <img class="item__user-avatar" src="{{ asset('images/platnut.png')}}" alt="User Image">
          </a>

          <ul class="dropdown-menu settings-menu dropdown-menu-right">
            <li>
              <a class="dropdown-item" href="">
                <i class="fa fa-user fa-lg"></i>
                Perfil
              </a>
            </li>
            <li>
             
                <form method="post" action="/logout">
                {{csrf_field()}}
                <button class="btn"> Cerrar sesion</button>
            </form>
              
            </li>
          </ul>
        </li>
      </ul>
    </header>
    <!-- Sidebar menu-->
    <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
    <aside class="app-sidebar">
      <div class="app-sidebar__user">
        <img class="app-sidebar__user-avatar" src="{{ asset('images/platnut.png')}}" alt="User Image">
      </div>

      <ul class="app-menu">
        <li class="treeview">
          <a class="app-menu__item @if ($title =='home' ) {{'active'}} @endif" href="{!! url('/admin/home'); !!}">
           <i class="fas fa-home"></i>
            <span class="app-menu__label pl-2">Inicio</span>
          </a>
        </li>
        <li class="treeview">
          <a class="app-menu__item @if ($title=='crearFruta') {{'active'}} @endif"  href="{!! url('/admin/crearFruta'); !!}" data-toggle="treeview">
          
            <i class="fab fa-foursquare"></i>
            <span class="app-menu__label pl-2">Frutas</span>
            <i class="treeview-indicator fa fa-angle-right"></i>
            
          </a>
          <ul class="treeview-menu">
            <li>
              <a class="treeview-item pl-4 " href="{!! url('/admin/crearFruta');!!}" >
                Agregar frutas
              </a>
            </li>
            <li>
              <a class="treeview-item  pl-4 @if ($title=='crearFruta') {{'active'}} @endif " href="{!! url('/admin/verFruta'); !!}">
                Listado de frutas
              </a>
            </li>
          </ul>
        </li>
        <li class="treeview">
          <a class="app-menu__item @if ($title=='crearVerdura') {{'active'}} @endif" href="!! url('/admin/crearVerdura'); !!}" data-toggle="treeview">
          
           <i class="fab fa-vimeo-square"></i>
            <span class="app-menu__label pl-2">Verduras</span>
            <i class="treeview-indicator fa fa-angle-right"></i>
          </a>
          <ul class="treeview-menu">
            <li>
              <a class="treeview-item pl-4" href="{!! url('/admin/crearVerduras'); !!}">
                Agregar verduras
              </a>
            </li>
            <li>
              <a class="treeview-item pl-4" href="{!! url('/admin/verVerdura'); !!}">
                Listado de verduras
              </a>
            </li>  
          </ul>
        </li>
        <li class="treeview">
          <a class="app-menu__item @if ($title=='crearPlanta') {{'active'}} @endif" href="{{!! url('/admin/crearPlanta'); !!}}" data-toggle="treeview">
          
            <i class="fab fa-product-hunt"></i>
            <span class="app-menu__label pl-2">Plantas</span>
            <i class="treeview-indicator fa fa-angle-right"></i>
          </a>
          <ul class="treeview-menu">
            <li>
              <a class="treeview-item pl-4" href="{!! url('/admin/crearPlantas'); !!}">
                Agregar planta medicinal
              </a>
            </li>
            <li>
              <a class="treeview-item pl-4" href="{!! url('/admin/verPlantas'); !!}">
                 Listado de plantas
              </a>
            </li>
          </ul>
        </li>
        <!-- <li class="treeview">
          <a class="app-menu__item" href="/admin/crearInfo_Nutrimental" data-toggle="treeview">
        
           <i class="fab fa-nutritionix"></i>
            <span class="app-menu__label pl-2">Informacion Nutrimental</span>
            <i class="treeview-indicator fa fa-angle-right"></i>
          </a>
          <ul class="treeview-menu">
            <li>
              <a class="treeview-item pl-4" href="{!! url('/admin/crearInfo_Nutrimental'); !!}">
                Agregar información
              </a>
            </li>
            <li>
              <a class="treeview-item pl-4" href="{!! url('/admin/verInfo_Nutrimental'); !!}">
                 Informacion nutrimental
              </a>
            </li>
          </ul>
        </li> -->
       <li class="treeview">
          <a class="app-menu__item @if($title=='crearPlatillo'){{'active'}} @endif" href="/admin/crearPlatillo" data-toggle="treeview">
           <i class="fas fa-utensils"></i>
            <span class="app-menu__label pl-2">Platillos</span>
            <i class="treeview-indicator fa fa-angle-right"></i>
          </a>
          <ul class="treeview-menu">
            <li>
              <a class="treeview-item pl-4" href="{!! url('/admin/crearPlatillo'); !!}">
                Agregar platillo
              </a>
            </li>
            <li>
              <a class="treeview-item pl-4" href="{!! url('/admin/verPlatillo'); !!}">
                 Listado de platillos
              </a>
            </li>
          </ul>
        </li>
         <li class="treeview">
          <a class="app-menu__item @if($title=='crearNutriologo') {{'active'}} @endif " href="/admin/crearNutriologo" data-toggle="treeview">
          <i class="fas fa-user-md"></i>
            <span class="app-menu__label pl-2">Nutriologos</span>
            <i class="treeview-indicator fa fa-angle-right"></i>
          </a>
          <ul class="treeview-menu">
            <li>
              <a class="treeview-item pl-4  " href="{!! url('/admin/crearNutriologo'); !!}">
                Agregar Nutriologo
              </a>
            </li>
            <li>
              <a class="treeview-item pl-4 " href="{!! url('/admin/verNutriologo'); !!}">
                 Listado de nutriologos
              </a>
            </li>
          </ul>
        </li>
      </ul>
    </aside>
    <main class="app-content">

       @yield('content')
       
       <title>plat nut</title>

    </main>

    <script src="{{ asset('js/jquery.js')}}"></script>
    <script src="{{ asset('js/admin/piexif.js')}}"></script>
    <script src="{{ asset('js/admin/sortable.js')}}"></script>
    <script src="{{ asset('js/admin/purify.js')}}"></script>
    <script src="{{ asset('js/admin/fileinput.js')}}"></script>
    <script src="{{ asset('js/admin/LANG.js')}}"></script>
    <script src="{{ asset('js/admin/theme.js')}}"></script>
    <script src="{{ asset('js/admin/fas/theme.js')}}"></script>
    <script src="{{ asset('js/popper.min.js')}}"></script>
    <script src="{{ asset('js/fontawesome-all.js')}}"></script>
    <script src="{{ asset('js/bootstrap.js')}}"></script>
    <script src="{{ asset('js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{ asset('js/admin/mainadmin.js')}}"></script>
    <script src="{{ asset('js/pace.min.js')}}"></script>
    <script src="{{ asset('js/adminsite.js')}}"></script>

              <!-- Chats js-->

   

    @yield('js')
  </body>
</html>
