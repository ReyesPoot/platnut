@extends('admin/layout/admin')
@section('content')
<!-- contenido de la pagina web -->
<div class="container" >
    <div class="cold-md-6">
        <h2>Eliminar fruta</h2>
            <form action="/admin/EliminarPadecidades" method="POST">
                {{csrf_field()}}

                <input type="text" value="{{$padecidades->Id_padecidad}}" name="id" hidden>
                <div class="form-group">
                    <label for="agregar frutas">Nombre de la padecidad</label>
                    <input type="text" value="{{$padecidades->nom_padecimiento}}" name="padecidad" class= "form-control placeholder" placeholder="Nombre de la padecidad">
                </div>
                <div class="form-group">
                    <label for="agregar frutas">Descripcion padecidad</label>
                    <textarea class="form-control" name="descripcion" rows="3" placeholder="Tipo de analisis">{{$padecidades->descripcion}}</textarea>
                </div>
                <div class="form-group">
                    <button type="submit" class= "btn btn-success">Eliminar</button>
                </div>
                
                 
         </form> 
    </div>    
</div> 
       
@endsection
@section('js')
<!-- archivos js dependientes de la vista -->
@endsection
