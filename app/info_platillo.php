<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class info_platillo extends Model 
{

protected $fillable= ['Id_platillo','Id_informacion','fecha_created','fecha_updated'];

//protected $primaryKey= 'Id_analisis';
protected $table= 'info_platillo';
const CREATED_AT ='fecha_created';
const UPDATED_AT ='fecha_updated';

//Relaciones

public function informacion(){
		return $this->belongsToMany('App/Informacion','Id_informacion','Id_platillo');

	}

}

 ?>