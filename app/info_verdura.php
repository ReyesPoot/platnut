<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class info_verdura extends Model 
{

protected $fillable= ['Id_verdura','Id_informacion','fecha_created','fecha_updated'];

//protected $primaryKey= 'Id_analisis';
protected $table= 'info_verdura';
const CREATED_AT ='fecha_created';
const UPDATED_AT ='fecha_updated';

//Relaciones

public function verdura(){
	return $this->belongsToMany('App/Verdura','Id_verdura','Id_verdura');

}
public function informacion(){
	return $this->belongsToMany('App/Informacion','Id_informacion','Id_informacion');

}

}

 ?>