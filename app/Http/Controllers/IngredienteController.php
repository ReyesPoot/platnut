<?php

namespace App\Http\Controllers;

use App\Platillo;
use App\Informacion;
use App\Ingrediente;
use App\Procedimiento;

use Illuminate\Http\Request; 

use App\Http\Controllers\Controller;

class IngredienteController extends Controller
{

   
//----------------------------administrador de ingredientes-------------------

    //funcion para agregar Ingredientes
    public function Editar($id_ingre_platillo,$id)
    {
        
        $ingredientes= Ingrediente::where('Id_ingrediente',$id)->first();
        $id_platillo= $id_ingre_platillo;
        return view ('Ingredientes/actualizarIngrediente', compact('ingredientes','id_platillo'));
       
    }

     public function Actualizar(Request $request)

     {

        $ingrediente= Ingrediente::where('Id_ingrediente',$request->id)->first();
       
        $ingrediente->ingrediente= $request->ingrediente;
        $ingrediente->cantidad=$request->cantidad;
        $ingrediente->unidad_medida=$request->unidad_medida;
       
         $ingrediente->save();
        // como obtengo el id del platillo y poder redireccionandolo al id del platillo
        // que se actualizo
        return redirect("/admin/ConsultarActualizar/$request->id_platillo");

     }

     public function EliminarIngrediente($id,$id_platillo)
     {

        $ingrediente = Ingrediente::find($id);

        $ingrediente->delete();

         return redirect('/admin/ConsultarEliminar/'. $id_platillo);

     }

     public function GuardarIngrediente(Request $request)
     {
        $Guardar= new ingrediente;
       
        $Guardar->ingrediente= $request->ingrediente;
        $Guardar->cantidad= $request->cantidad;
        $Guardar->unidad_medida= $request->unidad;
        $Guardar->save();
        //obteniendo el id del ingrediente
        $id_info=$Guardar->Id_ingrediente;

        $info=platillo::find($request->id);
        $info->ingredientes()->attach($id_info);

        return redirect('/admin/ConsultarPlatillos/'.$request->id); 

     } 
      

}