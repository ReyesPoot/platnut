<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Planta extends Model 
{

protected $fillable= ['imagen','Id_planta','nom_planta','descripcion_p','beneficios_p','fecha_created','fecha_updated'];

protected $primaryKey= 'Id_planta';
protected $table= 'planta';
const CREATED_AT ='fecha_created';
const UPDATED_AT ='fecha_updated';

public function informacion(){
		return $this->belongsToMany('App\Informacion', 'info_planta', 'Id_planta','Id_informacion');
	}
	public function platillo(){
		return $this->belongsToMany('App\planta', 'platillo_planta', 'Id_platillo','Id_planta');
	}

}

 ?>