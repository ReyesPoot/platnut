@extends('admin/layout/admin')

@section('content')

    @php $urlImagen= 'images/'. $fruta->imagen; @endphp
    
<div class="container" >
  <div class="card mb-4">
    <div class="row">
      <div class="col-md-4">
        <div class="card-body">
           <img src="{{ asset($urlImagen)}}" class="img-fluid" alt="{{ asset($urlImagen)}}">
        </div>
      </div>
      <div class="col-md-8">
          <div class="card-body">
            <h5 class="card-title">Fruta:</h5>
            <p>{{$fruta->nom_fruta}}</p>
            <h5 class="card-title">Descripcion</h5>
            <p>{{$fruta->descripcion_f}}</p>
          </div>
      </div>
    </div>
  <div class= "row">
      <div class="card-body">
        <div class= "col-md-12">
          <h5 class="card-title">Beneficio:</h5>
            <p class="card-text">{{$fruta->beneficios_f}}</p>
        </div>
      </div>
  </div>
  <div class="row">
    <div class= "card-body">
      <div class="col-md-6">
        <div class= "card-body">
            <h5 class="card-title">Valor nutrimental:</h5> 
      <p>
        <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
         Agregar
        </a>
      </p>
          <div class="collapse" id="collapseExample">
            <div class="card card-body">
               <form action="/admin/agregarInfoFruta" method="POST">

                    {{csrf_field()}}
                      <div class="row no-gutters">
                        
                         <input type="text" value="{{$fruta->Id_fruta}}" name="id" hidden>
                        <div class="col">
                            <label for="agregar">Informacion</label>
                            <input type="text" name="informacion" class= "form-control placeholder" placeholder="nombre..">
                        </div>
                        <div class="col">
                          <label for="agregar">Cantidad</label>
                          <input type="text" name="cantidad" class= "form-control placeholder" placeholder="Cantidad..">
                        </div>        
                        <div class="col">
                          <label for="agregar">Unidad</label>
                          <input type="text" name="unidad" class= "form-control placeholder" placeholder="Unidad..">
                        </div>  
                        <div class="form-group">
                          <input type="submit" value="Guardar" class= " btn btn-block btn-primary bnt-sm">

                        </div>   
                    </div>
                </form>
            </div>
          </div>
              <table class="table table-bordered">
              <thead class="thead-dark">
                <tr>
                  <th>Informacion</th>
                  <th>Cantidad</th>
                  <th>Unidad</th>
                </tr>
              </thead>
              <tbody>
                @foreach($fruta->informacion as $informacion)
                <tr>
                  <td>{{$informacion->informacion}}</td>
                  <td>{{$informacion->cantidad}}</td>
                  <td>{{$informacion->unidad_medida}}</td>
                </tr>
                 @endforeach
              </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="col-md-6">
        <div class="card-body">
          <h5 class="card-title">Platillos:</h5>
            <p>
              <a class="btn btn-primary" data-toggle="collapse" href="#Receta" role="button" aria-expanded="false" aria-controls="collapseExample">
               Agregar
              </a>
            </p>
          <div class="collapse" id="Receta">
            <div class="card card-body">
               <form action="/admin/agregarPlatilloFruta" method="POST">

                    {{csrf_field()}}

                     <input type="text" value="{{$fruta->Id_fruta}}" name="id" hidden>
                <div class="form-group">
                    <label for="agregar">Nombre del platillo</label>
                    <input type="text" name="nom_platillo" class= "form-control placeholder" placeholder="Nombre del platillo">
                </div>
                <div class="form-group">
                    <label for="agregar">Descripcion </label>
                    <input name="descripcion" class="form-control" rows="3" placeholder="escriba la descripcion del paltillo">
                </div>
                <div class="form-group">
                    <label for="agregar">Beneficios</label>
                    <input name="beneficios" class="form-control" rows="3" placeholder="escriba los beneficios del platillo">
                </div>
                <div class="form-group">
                    <input type="submit" value="Guardar" class= "btn btn-primary">
                </div>
                </form>
            </div>
          </div>

          @foreach($fruta->platillo as $platillo)
          <p>
            <a href="/admin/ConsultarPlatillos/{{$platillo->Id_platillo}}" class="btn-link">{{$platillo->nom_platillo}}</a>
          </p>
          @endforeach
        </div>
      </div>
    </div> 
    <center>
    <div class="card-body">
      <a href="/admin/verFruta" class="btn btn-outline-info">Regresar</a>
      <a href="/admin/EditarFruta/{{$fruta->Id_fruta}}" class="btn btn-success">Actualizar</a>
      <a href="/admin/EliminarFruta/{{$fruta->Id_fruta}}" class="btn btn-danger">Eliminar</a>
    </div>  
   </center> 
  </div>   
</div>
       
@endsection

@section('js')

 @endsection