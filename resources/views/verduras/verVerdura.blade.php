@extends('admin/layout/admin')
@section('content')
<!-- contenido de la pagina web -->
<div class="container">
  <div class="row">

    @foreach ($verdura as $verdura)

    @php $urlImagen= 'images/'. $verdura->imagen; @endphp
      
    <div class="col-md-3 my-2">
       <div class="card">
        <img src="{{ asset($urlImagen)}}" class="card-img-top" alt="{{ asset($urlImagen)}}">
          <div class="card-body">
             <h5 class="card-title">{{$verdura->nom_verdura}}</h5>
            <h5 class="card-title">Descripcion</h5>
            <p><th>{{ $verdura->descripcion_v }}</th></p>

            <a href="/admin/ConsultarVerdura/{{$verdura->Id_verdura}}" class="btn btn-block btn-primary">ver</a>
           
             
            
          </div>
      </div>
    </div> 

    @endforeach
  </div>
</div> 
     
@endsection
@section('js')
<!-- archivos js dependientes de la vista -->
@endsection