@extends('admin/layout/admin')

@section('content')

@php $urlImagen= 'images/'. $fruta->imagen; @endphp
    
<div class="container" >
  <div class="card mb-4">
    <div class="row">
      <div class="col-md-4">
          <img src="{{ asset($urlImagen)}}" class="img-fluid" alt="{{ asset($urlImagen)}}">
      </div>
      <div class="col-md-8">
          <div class="card-body">
            <h5 class="card-title">Fruta:</h5>
            <p>{{$fruta->nom_fruta}}</p>
            <h5 class="card-title">Descripcion</h5>
            <p>{{$fruta->descripcion_f}}</p>
          </div>
      </div>
    </div>
  <div class= "row">
      <div class="card-body">
        <div class= "col-md-12">
          <h5 class="card-title">Beneficio:</h5>
            <p class="card-text">{{$fruta->beneficios_f}}</p>
        </div>
      </div>
  </div>
  <div class="row">
    <div class= "card-body">
      <div class="col-md-8">
        <div class= "card-body">
            <h5 class="card-title">Valor nutrimental:</h5> 
              <table class="table table-bordered">
              <thead class="thead-dark">
                <tr>
                  <th>Informacion</th>
                  <th>Cantidad</th>
                  <th>Unidad</th>
                  <th>Acciones</th>    
                </tr>
              </thead>
              <tbody>
                @foreach($fruta->informacion as $informacion)
                <tr>
                  <td>{{$informacion->informacion}}</td>
                  <td>{{$informacion->cantidad}}</td>
                  <td>{{$informacion->unidad_medida}}</td>
                  <td>
                    <a href="/admin/EliminarInfoFruta/{{$informacion->Id_informacion}}/{{$fruta->Id_fruta}}" class="btn btn-block btn-danger btn -xs">Eliminar</a>
                  </td>
                </tr>
                 @endforeach
              </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="col-md-4">
        <div class="card-body">
          <h5 class="card-title">Receta:</h5>
          @foreach($platillo as $platillo)
          <a href="/admin/ConsultarPlatillos/{{$platillo->Id_platillo}}" class="btn-link">{{$platillo->nom_platillo}}</a>
          @endforeach
        </div>
      </div>

    </div>
    <center>
      <a href="/admin/verFruta" class="btn btn-outline-info">Regresar</a>
      <a href="/admin/EditarFruta/{{$fruta->Id_fruta}}" class="btn btn-success">Actualizar</a>
    </center>
  </div>   
</div>
       
@endsection

@section('js')

 @endsection