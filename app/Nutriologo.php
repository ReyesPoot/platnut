<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nutriologo extends Model 
{

protected $fillable= ['Id_nutriologo','nom_nutriologo','direccion','num_tel','fecha_created','fecha_updated'];

protected $primaryKey= 'Id_nutriologo';
protected $table= 'local_nutriologos';
const CREATED_AT ='fecha_created';
const UPDATED_AT ='fecha_updated';

}

 ?>