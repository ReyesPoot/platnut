
   @extends('user/layout/user')


  @section('content')

<div class="container">
    <div class="row">
        <div class="col-md-2 my-2">
            <div class="card text-white bg-primary mb-3">
                <h5 class="card-header">Frutas</h5>
                <div class="card-body">
                <h5 class="card-title">Cantidad</h5>
                <h2 class="text-center">{{$tot}}</h2>
                </div>
         </div>
        </div>

        <div class="col-md-2 my-2">
            <div class="card text-white bg-success mb-3">
                <h5 class="card-header">Verduras</h5>
                <div class="card-body">
                <h5 class="card-title">Cantidad</h5>
                <h2 class="text-center">{{$tol}}</h2>
                </div>
         </div>
        </div>
        <div class="col-md-2 my-2">
            <div class="card text-white bg-warning mb-3">
                <h5 class="card-header">Plantas</h5>
                <div class="card-body">
                <h5 class="card-title">Cantidad</h5>
                <h2 class="text-center">{{$total}}</h2>
                </div>
         </div>
        </div>
        <div class="col-md-2 my-2">
            <div class="card text-white bg-secondary mb-3">
                <h5 class="card-header">Platillos</h5>
                <div class="card-body">
                <h5 class="card-title">Cantidad</h5>
                <h2 class="text-center">{{$tototal}}</h2>
                </div>
         </div>
        </div>
        <div class="col-md-2 my-2">
            <div class="card text-white bg-dark mb-3">
                <h5 class="card-header">Nutriologos</h5>
                <div class="card-body">
                <h5 class="card-title">Total</h5>
                <h2 class="text-center">{{$toto}}</h2>
                </div>
         </div>
        </div>
    </div>  
</div>

<div class=container>
    <div class=row>
        <div class="col-md-9 my-3">

       <canvas id="myChart" ></canvas>
       <canvas id="informacion"></canvas>   

        </div>
    </div>
</div>





  @endsection
  @section('js')

  <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
  <script>
var ctx = document.getElementById('myChart');
var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'bar',

    // The data for our dataset
    data: {
        labels: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Maya', 'Junio', 'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
        datasets: [{
            label: 'Usuarios',
            backgroundColor: 'rgb(255, 99, 132)',
            borderColor: 'rgb(255, 99, 132)',
            data: [0, 10, 5, 2, 20, 30, 45,20,5,10,6,50]
        }]
    },

    // Configuration options go here
    options: {
         title: {
            display: true,
            text: 'Visita de Usuarios'
                }
    }
});
</script>
  @endsection