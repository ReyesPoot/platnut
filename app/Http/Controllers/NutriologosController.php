<?php

namespace App\Http\Controllers;

use App\Nutriologo;
use Illuminate\Http\Request; 

use App\Http\Controllers\Controller;

class NutriologosController extends Controller
{
  
    public function agregarNutriologo()
    {
        $title="crearNutriologo";
        return view ('Nutriologos/crearNutriologo',compact('title'));
    }
    // consultando nutriologos
    public function Consultar($id)
    {
        $title="crearNutriologo";
        $nutriologo=Nutriologo::where('Id_nutriologo',$id)->first();
        return view ('Nutriologos/Consultar',compact('title','nutriologo'));
        // dd($nutriologo);
    }

    public function store(Request $request)
    {

        $Guardar= new nutriologo;
        $Guardar->imagen=$request->imagen;
        $Guardar->nom_nutriologo= $request->nutriologo;
        $Guardar->direccion= $request->direccion;
        $Guardar->num_tel= $request->telefono;
      
        //dd($Guardar);
        $Guardar->save();
        return redirect('/admin/verNutriologo'); 
    }

    
    public function verNutriologo()
    {
        $title="crearNutriologo";
        $nutriologos= Nutriologo::get();
        return view ('Nutriologos/verNutriologo', compact('nutriologos','title'));
    }
    public function EditarNutriologo($id)
    {
        $title="crearNutriologo";
        $nutriologos= Nutriologo::where('Id_nutriologo',$id)->first();
        return view ('Nutriologos/actualizarNutriologo', compact('title','nutriologos'));

    }
    public function EliminarNutriologo($id)
    {
        $title="crearNutriologo";
        //dd($id);
        $nutriologos= Nutriologo::where('Id_nutriologo',$id)->first();
         return view ('Nutriologos/EliminarNutriologo', compact('title','nutriologos'));

    } 
    public function updateNutriologo(Request $request)
    {   //dd($_POST);
        
        $nutriologo= Nutriologo::where('Id_nutriologo',$request->id)->first();
        $nutriologo->nom_nutriologo=$request->nutriologo;
        $nutriologo->direccion=$request->direccion;
        $nutriologo->num_tel=$request->telefono;
        $nutriologo->imagen=$request->imagen;
        
        $nutriologo->save();
        
        return redirect('/admin/verNutriologo'); 

    }
    public function destroy(Request $request)
    {   
        
        $nutriologo= Nutriologo::where('Id_nutriologo',$request->id)->first();
        $nutriologo->delete();
        return redirect('/admin/verNutriologo');
        
    }

}