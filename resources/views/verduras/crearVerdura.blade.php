@extends('admin/layout/admin')
@section('content')
<!-- contenido de la pagina web -->
<div class="container" >
    

        <div class="cold-md-4">
            <h2>Agregar verduras nuevas</h2>
                <form action="/admin/agregarVerdura" method="POST">

                        {{csrf_field()}}

                        <div class= "file-loading">
                         <input id="id" type="file" name="imagen" accept="imagen/*">
                        </div>
                        <div class="form-group">
                            <label for="agragar verdura">Nombre del producto</label>
                            <input type="text" name="verdura" class= "form-control placeholder" placeholder="Nombre de la  verdura">
                        </div>
                        <div class="form-group">
                            <label for="agregar verdura">Descripcion </label>
                            <textarea name="descripcion" class="form-control" rows="3" placeholder="escriba la descripcion de la verduras"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="agregar verdura">Beneficios</label>
                            <textarea name="beneficios" class="form-control" rows="3" placeholder="escriba los beneficios de la verdura"></textarea>
                        </div>
                        
                        <div class="form-group">
                            <input type="submit" value="Guardar" class= "btn btn-primary">
                        </div>
            </form> 
        </div>  
   
</div>        
@endsection
@section('js')
<script type="text/javascript">
    $("#id").fileinput({
        theme: "fas",
        msgFileRequiered:"Ingresa una imagen",
        msgSizeTooLarge: "El archivo{name} ({Size} KB) excede el tamaño maximo permitido de {maxSize}",
        maxFileSize:1024,
        maxFileCount: 1,
        showRemove: false,
        showUpload: false,
        required: true,
        allowedFileExtensions:["jgp","png","jpeg"]

    });
</script>
<!-- archivos js dependientes de la vista -->
@endsection

       

