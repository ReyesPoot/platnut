@extends('admin/layout/admin')
@section('content')
<!-- contenido de la pagina web --> 
  @php $urlImagen= 'images/'. $platillo->imagen; @endphp

<div class="container">
  <div class="card mb-3">
    <form action="/admin/ConsultarActualizar" method="POST">  
    <div class="row ">
      <div class="col-md-4">
        <div class="card-body">
           <div class= "file-loading">
                  <input id="id" type="file" name="imagen" accept="imagen/*">
            </div>
        </div>
      </div>
        {{csrf_field()}}
      <input type="text" value="{{$platillo->Id_platillo}}" name="id" hidden>
      <div class="col-md-8">
        <div class="card-body">
          <h5 class="card-title">PLATILLO</h5>
            <input type="text" value="{{$platillo->nom_platillo}}" name="nom_platillo" class= "form-control placeholder" >
          <h5 class="card-title">DESCRIPCION</h5>
          <input type="text" value="{{$platillo->descripcion}}" name="descripcion" class= "form-control placeholder" >
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-8">
        <div class="card-body">
          <h5 class="card-title">BENEFICIOS</h5>
            <input type="text" value="{{$platillo->beneficios}}" name="beneficios" class= "form-control placeholder" >
        </div>
      </div>
    </div>  
    <div class="card-body">
            <button type="submit" class= "btn btn-success">Actualizar</button>
    </div> 
    </form>
    <div class= "row">
      <div class="col-md-6">
        <div class="card-body">
          <h5 class="card-title">VALOR NUTRICIONAL</h5>
            <table class="table table-bordered">
              <thead class="thead-dark">
                <tr>
                  <th>Nutrientes</th>
                  <th>cantidad</th>
                  <th>Unidad</th>
                  <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                  @foreach($platillo->informacion as $informacion)
                  <tr>
                    <td>{{$informacion->informacion}}</td>
                    <td>{{$informacion->cantidad}}</td>
                    <td>{{$informacion->unidad_medida}}</td>
                    <td>
                      <a href="/admin/EditarInfoPlatillo/{{$platillo->Id_platillo}}/{{$informacion->Id_informacion}}" class="btn btn-block btn-success btn -xs">Actualizar</a>

                    </td>
                  </tr>
                  @endforeach
                </tbody>
            </table>
        </div>
      </div>
       <div class="col-md-6">
           <div class="card-body">
          <h5 class="card-title">INGREDIENTES: </h5>
       
            @foreach($platillo->ingredientes as $ingredientes) 
                <p>{{$ingredientes->cantidad}} {{$ingredientes->unidad_medida}} de {{$ingredientes->ingrediente}} 
                  <a href="/admin/actualizarIngrediente/{{$platillo->Id_platillo}}/{{$ingredientes->Id_ingrediente}}" class="btn btn-success btn -xs">Actualizar</a>
                </p>
                @endforeach
            </div>
          </div>
        </div>
          <div class="row">
            <div class="col-md-6">
                <div class="card-body">
                  <h5 class="card-title">PREPARACION</h5>
                    <?php $c=1;  ?>
                     @foreach($platillo->instrucciones as $instrucciones)
                        <p> paso {{$c++}}:{{$instrucciones->instrucciones}} 
                          <a href="/admin/actualizarInstrucciones/{{$platillo->Id_platillo}}/{{$instrucciones->Id_instrucciones}}" class="btn btn-success btn -xs">Actualizar</a>
                        </p>
                     @endforeach
                     
                </div> 
            </div>
          </div>       
        <center>
            <a href="/admin/verPlatillo" class="btn btn-outline-info">Regresar</a>
            <a href="/admin/ConsultarEliminar/{{$platillo->Id_platillo}}" class="btn btn-danger">Eliminar</a>
        </center>
      </div>
    </div>
  

@endsection
@section('js')
<script type="text/javascript">
 
    $("#id").fileinput({
        theme: "fas",
        overwriteInitial:true,
        autoReplace:true,
        initialPreviewAsData:true,
        @if($platillo->imagen)
          initialPreview: [

            '{{ asset("images/".$platillo->imagen) }}'
          ],

          initialPreviewConfig: [
            {type: "image", caption: "logopropueta.png"},
          ],
        @endif

        msgFileRequiered:"Ingresa una imagen",
        msgSizeTooLarge: "El archivo{name} ({Size} KB) excede el tamaño maximo permitido de {maxSize}",
        maxFileSize:1024,
        maxFileCount: 1,
        showRemove: false,
        showUpload: false,
        required: true,
        allowedFileExtensions:["jgp","png","jpeg"]

    });
</script>

<!-- archivos js dependientes de la vista -->
@endsection
