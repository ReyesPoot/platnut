<?php

namespace App\Http\Controllers;
use App\Verdura;
use App\Platillo;
use App\Informacion;
use Illuminate\Http\Request; 
use App\Ingrediente;
use App\Http\Controllers\Controller;



class VerdurasController extends Controller
{
    public function Consultar($id)
        {
            // $verdura=Verdura::where('Id_verdura',$id)->first();
            
            $title="crearVerdura";

            $verdura=Verdura::where('Id_verdura',$id)->first();
            $Consulta = Verdura::find($id)->informacion()->get();
            $platillo_verdura=Verdura::find($id)->platillo()->get();
            // dd($platillo_verdura); 
         
            
            $ingrediente=Ingrediente::where('Id_ingrediente',$id)->get();
            return view ('verduras/ConsultarVerdura', compact('verdura','Consulta','ingrediente','platillo_verdura','title'));

        }  
          
  public function agregarVerduras()
    {
        $title="crearVerdura";

        return view ('verduras/crearVerdura', compact('title'));

    }  
    public function ver()
    {
       $title="crearVerdura";
         $verdura= Verdura::get();
        return view ('verduras/verVerdura',compact('verdura','title'));

    }

     public function EditarVerdura($id)
    {
        $title="crearVerdura";
        $verdura= Verdura::where('Id_verdura',$id)->first();
        $platillo=Platillo::where('Id_platillo',$id)->get();
        return view ('verduras/actualizarVerdura', compact('title','verdura','platillo'));

    }
     public function EliminarVerdura($id)
    {
        $title="crearVerdura";
       // dd($id);
        $verdura= Verdura::where('Id_verdura',$id)->first();
        $platillo=Platillo::where('Id_platillo',$id)->get();
         return view ('verduras/EliminarVerdura', compact('title','verdura','platillo'));

    } 
    public function store(Request $request)
    {
    	
        $Guardar= new Verdura;
        $Guardar->imagen= $request->imagen;
        $Guardar->nom_verdura= $request->verdura;
        $Guardar->descripcion_v= $request->descripcion;
        $Guardar->beneficios_v= $request->beneficios;
       
        $Guardar->save(); 

        return redirect('/admin/verVerdura'); 
        //dd($Guardar);
    }
    public function updateVerdura(Request $request)
    {   //dd($_POST);
        
        $verduras= Verdura::where('Id_verdura',$request->id)->first();
        $verduras->imagen=$request->imagen;
        $verduras->nom_verdura=$request->verdura;
        $verduras->descripcion_v=$request->descripcion;
        $verduras->beneficios_v=$request->beneficios;
       
        $verduras->save();
        
        return redirect('/admin/verVerdura'); 

    }

     public function destroy(Request $request)
    {   
        
        $verduras= Verdura::where('Id_verdura',$request->id)->first();
        $verduras->delete();
        return redirect('/admin/verVerdura');
        
    }
}