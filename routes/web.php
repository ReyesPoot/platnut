<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//HASEO DE CONTRASEÑAS
 // Route::get('/hashpass', function()
 // {
 //   $password = Hash::make('12345678');
 //   dd($password);

 // });
// Inicio de session
Route::get('/acceso', 'Auth\LoginController@showLoginForm');
Route::post('/acceso', 'Auth\LoginController@login');



Route::group(['middleware' => 'is_admin'], function() {

 	Route::post('/logout','Auth\LoginController@logout');

	// rutas de las vistas administracion de frutas

	Route::get('/admin/home','FrutaController@Home');
	Route::get('/user/home','FrutaController@user');

	// rutas para agregar frutass
	Route::get('/admin/crearFruta','FrutaController@agregar');
	Route::post('/admin/crearFruta','FrutaController@store') ;
	// rutas para ver el listado de frutas
	Route::get('/admin/verFruta','FrutaController@ver');
	// rutas para editar las frutas
	Route::get('/admin/EditarFruta/{id}','FrutaController@Editar');
	Route::post('/admin/EditarFrutas','FrutaController@update');
	// ruta para eliminar la frutas
	Route::get('/admin/EliminarFruta/{id}','FrutaController@Eliminar');
	Route::post('/admin/EliminarFrutas','FrutaController@destroy');

	//pasos de preparacion
	Route::get('/admin/prepararacion','FrutaController@preparar');


	//--------- ver fruta consultar-------------

	Route::get('/admin/ConsultarFruta/{id}','FrutaController@Consultar');


	//--------------- rutas para la administracion de verduras-----------

	Route::get('/admin/crearVerduras','VerdurasController@agregarVerduras');
	Route::post('/admin/agregarVerdura','VerdurasController@store');
	// ruta para visualizar las frutas agregadas
	Route::get('/admin/verVerdura','VerdurasController@ver');
	// ruta por la cual se carga la opcion de editar
	Route::get('/admin/EditarVerdura/{id}','VerdurasController@EditarVerdura');
	Route::post('/admin/EditarVerduras','VerdurasController@updateVerdura');
	// rutas para eliminar la verdura
	Route::get('/admin/EliminarVerdura/{id}','VerdurasController@EliminarVerdura');
	Route::post('/admin/EliminarVerduras','VerdurasController@destroy');

// ------------------para consultar Verduras-----------------------

	Route::get('/admin/ConsultarVerdura/{id}','VerdurasController@Consultar');
	// rutas para administrar las plantas
	Route::get('/admin/crearPlantas','PlantaController@agregar');
	Route::post('/admin/agregar','PlantaController@store');
	// ruta para visualizar los registros agregados
	Route::get('/admin/verPlantas','PlantaController@ver');
	// ruta por la cual se carga la opcion de editar
	Route::get('/admin/EditarPlanta/{id}','PlantaController@EditarPlanta');
	Route::post('/admin/EditarPlantas','PlantaController@updatePlanta');
	// rutas para eliminar la verdura
	Route::get('/admin/EliminarPlanta/{id}','PlantaController@EliminarPlanta');
	Route::post('/admin/EliminarPlantas','PlantaController@destroy');

	// consultar Plantas

	Route::get('/admin/ConsultarPlanta/{id}','PlantaController@Consultar');

	// --------------administracion de la informacion nutrimental---------

	Route::get('/admin/crearInfo_Nutrimental','Info_NutController@agregar');
	Route::post('/admin/agregarInfoFruta','Info_NutController@storeFruta');
	Route::post('/admin/agregarInfoVerdura','Info_NutController@storeVerdura');
	Route::post('/admin/agregarInfoPlanta','Info_NutController@storePlanta');
	Route::post('/admin/agregarInfoPlatillo','Info_NutController@storePlatillo');

	// -----------------Rutas para la administracion de la informacion nutrimental de los productos--------------

	Route::get('/admin/verInfo_Nutrimental','Info_NutController@ver');
	Route::get('/admin/EditarInfoFruta/{id_fruta}/{id}','Info_NutController@EditarInformacionFruta');
	Route::get('/admin/EditarInfoVerdura/{id_verdura}/{id}','Info_NutController@EditarInformacionVerdura');
	Route::get('/admin/EditarInfoPlanta/{id_planta}/{id}','Info_NutController@EditarInformacionPlanta');
	Route::get('/admin/EditarInfoPlatillo/{id_platillo}/{id}','Info_NutController@EditarInformacionPlatillo');
	Route::post('/admin/EditarInfos','Info_NutController@updateInformacion');
	Route::post('/admin/EditarInfoVerdura','Info_NutController@updateInformacionVerdura');
	Route::post('/admin/EditarInfoPlanta','Info_NutController@updateInformacionPlanta');
	Route::post('/admin/EditarInfoPlatillo','Info_NutController@updateInformacionPlatillo');

	// ------------Eliminando informacion nutrimental de los productos.-------------

	Route::get('/admin/EliminarInfoFruta/{id}/{id_fruta}','Info_NutController@EliminarInfoFruta');
	Route::get('/admin/EliminarInfoVerdura/{id}/{id_verdura}','Info_NutController@EliminarInfoVerdura');
	Route::get('/admin/EliminarInfoPlanta/{id}/{id_planta}','Info_NutController@EliminarInfoplanta');
	Route::get('/admin/EliminarInfoPlatillo/{id}/{id_platillo}','Info_NutController@EliminarInfoPlatillos');

	// rutas de administracion de los platillos

	Route::get('/admin/crearPlatillo','PlatilloController@agregarPlatillo');
	Route::post('/admin/agregarPlatilloFruta','PlatilloController@storeFruta');
	Route::post('/admin/agregarPlatilloVerdura','PlatilloController@storeVerdura');
	// ruta para visualizar los registros agregados
	Route::get('/admin/verPlatillo','PlatilloController@verPlatillo');

	// ruta por la cual se carga la opcion de editar
	//Route::get('/admin/ConsultarPlatillos/{id}','PlatilloController@EditarPlatillo');

	// rutas para eliminar la verdura
	//Route::get('/admin/EliminarPlatillo/{id}','PlatilloController@EliminarPlatillo');
	//Route::post('/admin/EliminarPlatillos','PlatilloController@destroy');


	//consultando platillos
	Route::get('/admin/ConsultarPlatillos/{id}','PlatilloController@Consultar');
	Route::get('/admin/ConsultarEliminar/{id}','PlatilloController@EliminarConsultar');
	Route::get('/admin/ConsultarActualizar/{id}','PlatilloController@EditarConsultar');
	Route::post('/admin/ConsultarActualizar','PlatilloController@updatePlatillo');

	// agragar ingredientes de los platillos

	Route::post('/admin/agregarIngrediente','IngredienteController@GuardarIngrediente');

	//Actualizar Ingredientes
	
	Route::get('/admin/actualizarIngrediente/{id_platillo}/{id}','IngredienteController@Editar');
	Route::post('/admin/editarIngrediente','IngredienteController@Actualizar');

	//Eliminar Ingredientes
	Route::get('/admin/EliminarIngrediente/{id}/{id_platillo}','IngredienteController@EliminarIngrediente');

	// instrucciones para agregar los pasos de preparacion de los platillos.

	Route::post('/admin/agregarInstrucciones','instruccionesController@GuardarInstrucciones');
	Route::get('/admin/actualizarInstrucciones/{id_platillo}/{id}','instruccionesController@Editar');
	Route::post('/admin/editarInstrucciones','instruccionesController@Actualizar');
    Route::get('/admin/EliminarInstrucciones/{id}/{id_platillo}','instruccionesController@EliminarInstrucciones');


						// administracion de nutrilogos

	Route::get('/admin/crearNutriologo','NutriologosController@agregarNutriologo');
	Route::post('/admin/agregarNutriologos','NutriologosController@store');
	// ruta para visualizar los registros agregados
	Route::get('/admin/verNutriologo','NutriologosController@verNutriologo');
	// ruta por la cual se carga la opcion de editar
	Route::get('/admin/EditarNutriologo/{id}','NutriologosController@EditarNutriologo');
	Route::post('/admin/EditarNutriologos','NutriologosController@updateNutriologo');
	// rutas para eliminar los registros
	Route::get('/admin/EliminarNutriologo/{id}','NutriologosController@EliminarNutriologo');
	Route::post('/admin/EliminarNutriologos','NutriologosController@destroy');

	Route::get('/admin/Consultar/{id}','NutriologosController@Consultar');



});

//@if ($title =='Home' ) {{'active'}} @endif"
