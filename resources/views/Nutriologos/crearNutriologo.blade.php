@extends('admin/layout/admin')
@section('content')
<!-- contenido de la pagina web -->
<div class="container" >
        <div class="col-md-4">
            <h2>Agregar Nutriologo</h2>
            <form action="/admin/agregarNutriologos" method="POST">
                
                {{csrf_field()}}

                <div class= "file-loading">
                    <input id="id" type="file" name="imagen" accept="imagen/*">
                </div>
                <div class="form-group">
                    <label for="agregar">Nombre del nutriologo</label>
                    <input type="text" name="nutriologo" class= "form-control placeholder" placeholder="Nombre del nutriologo">
                </div>
                <div class="form-group">
                    <label for="agregar">Direccion del nutriologo </label>
                    <textarea name="direccion" class="form-control" rows="3" placeholder="Escriba el nombre del nutrilogo"></textarea>
                </div>
                <div class="form-group">
                    <label for="agregar">Numero de Telefono</label>
                    <textarea name="telefono" class="form-control" rows="3" placeholder="Escriba el numero telefonico del contacto"></textarea>
                </div>
                <div class="form-group">
                    <input type="submit" value="Guardar" class= "btn btn-primary">
                </div>
                 

             </form> 
         </div>    
</div>        
@endsection
@section('js')
<!-- archivos js dependientes de la vista -->

<script type="text/javascript">
    $("#id").fileinput({
        theme: "fas",
        msgFileRequiered:"Ingresa una imagen",
        msgSizeTooLarge: "El archivo{name} ({Size} KB) excede el tamaño maximo permitido de {maxSize}",
        maxFileSize:1024,
        maxFileCount: 1,
        showRemove: false,
        showUpload: false,
        required: true,
        allowedFileExtensions:["jgp","png","jpeg"]

    });

</script>
@endsection

       

