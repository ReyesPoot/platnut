@extends('admin/layout/admin')
@section('content')
<!-- contenido de la pagina web -->
<div class="container" >
    <div class="cold-md-6">
        <h2>Eliminar Ingrediente </h2>
            <form action="/admin/EliminarInstrucciones" method="POST">

                {{csrf_field()}}
                                
                                <input type="text" value="{{$instrucciones->Id_instrucciones}}" name="id" hidden>     
                            
                                <div class="form-group">
                                    <label for="agregar verduras">Instrucciones de preparacion</label>
                                    <input type="text" value="{{$instrucciones->instrucciones}}" name="ingrediente" class= "form-control placeholder" placeholder="Nombre..">
                                </div>
                                
                                
                <div class="form-group">
                    <button type="submit" class= "btn btn-outline-danger btn-block">Eliminar</button>
                    <!-- <button type="submit" class= "btn btn-danger">Eliminar</button> -->
                </div>
         </form> 
    </div>    
</div>      
@endsection
@section('js')

@endsection