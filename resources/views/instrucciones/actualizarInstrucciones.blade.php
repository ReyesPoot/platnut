@extends('admin/layout/admin')
@section('content')
<!-- contenido de la pagina web -->
<div class="container" >
    <div class="cold-md-6">
                <h2>Actualizar instrucciones de preparacion del platillo</h2>
        <form action="/admin/editarInstrucciones" method="POST">

                {{csrf_field()}}
            <input type="text" value="{{$instrucciones->Id_instrucciones}}" name="id" hidden>
            <input type="text" value="{{$id_platillo}}" name="id_platillo" hidden>
                <div class="form-group">
                    <label for="agregar verduras">Instrucion</label>
                    <input type="text" value="{{$instrucciones->instrucciones}}" name="instrucciones" class= "form-control placeholder" >
                </div>
                <center>       
                    <div class="form-group">
                        <button type="submit" class= "btn btn-success">
                             Actualizar
                        </button>
                    </div>
                </center>              
            
        </form> 
    </div>    
</div> 
       
@endsection
@section('js')

@endsection