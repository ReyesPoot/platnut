@extends('admin/layout/admin')
@section('content')
 @php $urlImagen= 'images/'. $verdura->imagen; @endphp
    
<div class="container" >
  <div class="card mb-4">
    <div class="row">
      <div class="col-md-4">
          <img src="{{ asset($urlImagen)}}" class="img-fluid" alt="{{ asset($urlImagen)}}">
      </div>
      <div class="col-md-8">
          <div class="card-body">
            <h5 class="card-title">Verdura:</h5>
            <p>{{$verdura->nom_verdura}}</p>
            <h5 class="card-title">Descripcion</h5>
            <p>{{$verdura->descripcion_v}}</p>
          </div>
      </div>
    </div>

  <div class= "row">
      <div class="card-body">
        <div class= "col-md-5">
          <h5 class="card-title">Beneficio:</h5>
            <p class="card-text">{{$verdura->beneficios_v}}</p>
        </div>
      </div>
  </div>
  <div class="row">
      <div class="col-md-8">
        <div class= "card-body">
            <h5 class="card-title">Valor nutrimental:</h5> 

              <table class="table table-bordered">
              <thead class="thead-dark">
                
                <tr>
                  <th>Componente</th>
                  <th>Cantidad</th>
                  <th>Unidad</th>
                  <th>Acciones</th>
                  
                </tr>
              </thead>
              <tbody>
                @foreach($verdura->informacion as $informacion)
                <tr>
                  <td>{{$informacion->informacion}}</td>
                  <td>{{$informacion->cantidad}}</td>
                  <td>{{$informacion->unidad_medida}}</td>
                  <td> 

                     <a href="/admin/EliminarInfoVerdura/{{$informacion->Id_informacion}}/{{$verdura->Id_verdura}}" class="btn btn-block btn-danger btn -xs">Eliminar</a>

                  </td>
                </tr>
                 @endforeach
              </tbody>
          </table>
        </div>
      </div>
    
      <div class="col-md-4">
        <div class="card-body">
          <h5 class="card-title">Receta:</h5>
          @foreach($verdura->platillo as $platillo)
          <a href="/admin/ConsultarPlatillos/{{$platillo->Id_platillo}}" class="btn-link">{{$platillo->nom_platillo}}</a>
          @endforeach
        </div>
      </div>
    </div>
    <center>
      <div class="card-body">
        <a href="/admin/verVerdura" class="btn btn-primary">Regresar</a>
        <a href="/admin/EliminarVerdura/{{$verdura->Id_verdura}}" class="btn btn-success">Actualizar</a>
      </div>
     </center>
  </div>
</div> 

@endsection
@section('js')
<!-- archivos js dependientes de la vista -->
@endsection
