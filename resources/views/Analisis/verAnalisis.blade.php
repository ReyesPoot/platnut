@extends('admin/layout/admin')
@section('content')
<!-- contenido de la pagina web -->
<div class="container" >
    <div class="cold-md-6">
        <h2>Listado de los analisis</h2>
        <div>
          <div class= "panel panel-default">
          </div>
           <table class= "table table_responsive table-bordered">
               <thead>

                    <th>ID</th>
                    <th>Analisis</th>
                    <th>Tipo</th>
                    <th>Acciones</th> 
               </thead>
               <tbody>
              @foreach ($analisis as $analisis_quimicos)
                    <tr>
                    <th>{{ $analisis_quimicos->Id_analisis}} </th>
                    <th>{{ $analisis_quimicos->nom_analisis}}</th>
                    <th>{{ $analisis_quimicos->tipo}}</th>
                     <th>
                       <a href="/admin/EditarAnalisis/{{$analisis_quimicos->Id_analisis}}" class="btn btn-warning btn -xs" >Editar</a>
                        <a href="/admin/EliminarAnalisis/{{$analisis_quimicos->Id_analisis}}" class="btn btn-danger btn -xs">Eliminar</a>
                     </th>
                    </tr>  

                @endforeach 

               </tbody>
           </table>
    
        </div>
    </div>    
</div> 
       
@endsection
@section('js')
<!-- archivos js dependientes de la vista -->
@endsection