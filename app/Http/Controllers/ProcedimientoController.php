<?php

namespace App\Http\Controllers;

use App\Procedimiento;
use App\Platillo;
use Illuminate\Http\Request; 

use App\Http\Controllers\Controller;

class ProcedimientoController extends Controller
{

    public function agregarProcedimiento()
    {
        
       $platillo=Platillo::get();
        return view ('Procedimientos/crearProcedimiento',compact('platillo'));
      
    }
     public function store(Request $request)
    {

        $Guardar= new Procedimiento;

        $Guardar->Id_procedimiento=$request->id;
        $Guardar->paso_1= $request->paso_1;
        $Guardar->paso_2= $request->paso_2;
        $Guardar->paso_3= $request->paso_3;
        $Guardar->paso_4= $request->paso_4;
        $Guardar->paso_5= $request->paso_5;
        $Guardar->paso_6= $request->paso_6;
       // dd($Guardar);
        $Guardar->save();
       // return redirect(''); 
    }

}