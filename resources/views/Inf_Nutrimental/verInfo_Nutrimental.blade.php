@extends('admin/layout/admin')
@section('content')
<!-- contenido de la pagina web -->
<div class="table table-striped" >
    <div class="cold-md-6">
        <h2>Informacion nutrimental de cada producto</h2>
        <div>
          <div class= "panel panel-default">
           
          </div>
           <table class= "table table_responsive table-bordered">
               <thead class=thead-dark>

                    <th>Contador</th>
                    <th>Producto</th>
                    <th>Cantidad</th>
                    <th>unidad_medida</th>
                    
                    <th>Acciones</th>
                    
               </thead>
             
               <tbody>
                  <?php $c=1;  ?>
                @foreach ($informacion as $informacion_nutri)
                    <tr>
                    <th>{{ $c++}}</th>
                    <th>{{ $informacion_nutri->nom_producto}}</th>
                    <th>{{ $informacion_nutri->cantidad}}</th>
                    <th>{{ $informacion_nutri->unidad_medida}} </th>
                    
                  
                     <th>
                       

                        <a href="/admin/EliminarInfo/{{$informacion_nutri->Id_informacion}}" class="btn btn-block btn-danger btn -xs">Eliminar</a>
                       
                     </th>
                    </tr>  

                @endforeach 

               </tbody>
           </table>
    
        </div>
    </div>    
</div> 
       
@endsection
@section('js')
<!-- archivos js dependientes de la vista -->
@endsection