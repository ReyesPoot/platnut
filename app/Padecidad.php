<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Padecidad extends Model 
{

protected $fillable= ['Id_padecidad','nom_padecimiento','descripcion','fecha_created','fecha_updated'];

protected $primaryKey= 'Id_padecidad';
protected $table= 'padecidad';
const CREATED_AT ='fecha_created';
const UPDATED_AT ='fecha_updated';

//Relaciones

public function padecidad(){
	return $this->hasOne('App/Padecidad','Id_padecidad','Id_informacion');
	
}

public function analisis(){
	return $this->hasOne('App/Analisis','Id_analisis','Id_padecidad');
	
}



}

 ?>