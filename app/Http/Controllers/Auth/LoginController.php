<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\ValidationException;

use Illuminate\Http\Request;


Class LoginController extends Controller
{

	Use AuthenticatesUsers;

	//muestra el formulario de inicio de sesion
    public function showLoginForm()
    {
        if(Auth::guard('admin')->check()){

            $user = Auth::user();

           if($user->tipo_user == "admin") {
                return redirect('admin/home');
           } else {
                return redirect('user/home');
           }
        } else {
            return view('auth.login');
        }
    }

		protected function guard()
		{
			return Auth::guard('admin');
		}

		public function username()
	  {
			return 'nom_usuario';
	  }

	//logica de logeo
    public function login(Request $request) {
        $this->validateLogin($request);
        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        /*if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }*/

        $credentials = $request->only('nom_usuario', 'password');

        if (Auth::attempt($credentials)) {
            $user = Auth::user();
            if($user->tipo_user == "admin") {
                return redirect('admin/home');
            } else {
                return redirect('user/home');
            } 
        }
        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        //$this->incrementLoginAttempts($request);
        return $this->sendFailedLoginResponse($request);
    }

    //invocado en metodo login, como parte de la logica de logeo
    //para validar los campos del formulario
    protected function validateLogin(Request $request){
        //Definimos los mensajes a mostrar segun la validación
        $messages = [
            'required' => 'El campo :attribute  es requerido.',
            'nom_usuario'  => 'El :attribute debe ser un correo electrónico valido.',
            'min'    => [
                'string'  => 'El :attribute debe tener al menos :min caracteres.'],
            /*'g-recaptcha-response' => [
                'required' => 'Por favor verifica que no eres un robot.',
                'captcha' => 'Error de captcha! Intenta despues o contacte con el administrador.',
            ],*/
            'auth' => [
                'failed' => 'Estas credenciales no concuerdan con nuestros registros.',
                'throttle' => 'Demasiados intentos fallidos. Por favor intentelo en :seconds segundos.',
            ],
        ];
        $this->validate($request, [
            $this->username() => 'required|string|email',
            'password' => 'required|string|min:8',
            //'g-recaptcha-response' => 'required|captcha'
        ], $messages);
    }


    //Sobreescribimos metodo del Trait
    //Metodo para el manejo de error en caso de no encontrarse datos en la bd
    protected function sendFailedLoginResponse(Request $request){
        throw ValidationException::withMessages([
            $this->username() => ['Estas credenciales no concuerdan con nuestros registros.'],
        ]);
    }

    //invocado en metodo login, como parte de la logica de logeo
    //finaliza el proceso de logeo
    //Metodo para redireccionar al usuario autentificado segun el tipo de usuario
    protected function authenticated(Request $request, $user){

    	return redirect('/admin/home');
    }


	public function logout(Request $Request)
	{
		Auth::logout();
		Auth::guard('admin')->logout();
		return redirect('/acceso');
		Session::flash('msg-cerrar','Tu sesion ha sido cerrada');
	}

}
