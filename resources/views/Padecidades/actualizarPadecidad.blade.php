@extends('admin/layout/admin')
@section('content')
<!-- contenido de la pagina web -->
<div class="container" >
    <div class="cold-md-6">
        <h2>Actualizar frutas</h2>
            <form action="/admin/EditarPadecidades" method="POST">

                {{csrf_field()}}
                <input type="text" value="{{$padecidades->Id_padecidad}}" name="id" hidden>
                <div class="form-group">
                    <label for="agregar frutas">Nombre de la padecimiento</label>
                    <input type="text" value="{{$padecidades->nom_padecimiento}}" name="padecidad" class= "form-control placeholder" placeholder="Nombre del padecimiento">
                </div>

                <div class="form-group">
                    <label for="agregar frutas">Descripcion de la fruta</label>
                    <textarea class="form-control" name="descripcion" rows="3" placeholder="escriba la descripcion de la fruta">{{$padecidades->descripcion}}</textarea>
                </div>
                <div class="form-group">
                    <button type="submit" class= "btn btn-success">actualizar</button>
                    <!-- <button type="submit" class= "btn btn-danger">Eliminar</button> -->
                </div>
                
                 
         </form> 
    </div>    
</div> 
       
@endsection
@section('js')
<!-- archivos js dependientes de la vista -->
@endsection
