@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-primary">
                <div class="panel-heading text-center">LOGIN</div>
                    <div class="panel-body">
                        @if(Session::has('msg-error'))
                            <div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <p>{!!Session::get('msg-error')!!}</p>
                            </div>
                        @endif
                    <form class="form-horizontal" method="POST">
                        {{ csrf_field() }}
                        @if ($errors->has('nom_usuario'))
                            <span class="help-block">
                                <strong style="color: red">{{ $errors->first('nom_usuario') }}</strong>
                            </span>
                        @endif
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong style="color: red">{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                        <div class="form-group">
                            <label for="text" class="col-sm-4 control-label">Usuario</label>
                            <div class="col-md-4">
                                <input id="usuario" type="text" class="form-control" name="nom_usuario" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password" class="col-sm-4 control-label">Contraseña</label>
                            <div class="col-sm-4">
                                <input id="password" type="password" class="form-control" name="password" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4 col-md-offset-4">
                                <button type="submit" class="btn btn-success btn-lg btn-block">
                                    Iniciar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
