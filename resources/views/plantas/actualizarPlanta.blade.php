@extends('admin/layout/admin')
@section('content')
<!-- contenido de la pagina web -->

<div class="container" >
  <div class="card mb-4">
    <form action="/admin/EditarPlantas" method="POST">     
    <div class="row">
      <div class="col-md-4">
         <div class= "file-loading">
              <input id="id" type="file" name="imagen" value="{{$planta->imagen}}" accept="imagen/*">
          </div> 
      </div>       
              {{csrf_field()}}

          <input type="text" value="{{$planta->Id_planta}}" name="id" hidden>
          <div class="col-md-8">
            <div class="card-body">
                <h5 class="card-title">Planta</h5>
                <input type="text" value="{{$planta->nom_planta}}" name="planta" class= "form-control placeholder" >
                <h5 class="card-title">Descripcion:</h5>
                <input type="text" value="{{$planta->descripcion_p}}" name="descripcion" class= "form-control placeholder" >
            </div>
          </div>
        </div>
          <div class="row">
            <div class="card-body">
              <div class="col-md-7">
                 <h5 class="card-title">Beneficios:</h5>
                 <input type="text" value="{{$planta->beneficios_p}}" name="beneficios" class= "form-control placeholder" >
              </div>
            </div>
          </div>    
          <div class="card-body">
            <button type="submit" class= "btn btn-success">Actualizar</button>
          </div>            
      </form> 
      <div class="row">
      <div class="col-md-8">
        <div class= "card-body">
            <h5 class="card-title">Valor nutrimental:</h5> 

              <table class="table table-bordered">
              <thead class="thead-dark">
                
                <tr>
                  <th>Componente</th>
                  <th>Cantidad</th>
                  <th>Unidad</th>
                  <th>Acciones</th>
                  
                </tr>
              </thead>
              <tbody>
                @foreach($planta->informacion as $informacion)
                <tr>
                  <td>{{$informacion->informacion}}</td>
                  <td>{{$informacion->cantidad}}</td>
                  <td>{{$informacion->unidad_medida}}</td>
                  <td>
                <a href="/admin/EditarInfoPlanta/{{$planta->Id_planta}}/{{$informacion->Id_informacion}}" class="btn btn-block btn-success btn -xs">Actualizar</a>

                </tr>
                 @endforeach
              </tbody>
          </table>
        </div>
      </div>
      <!-- <div class="col-md-4">
        <div class="card-body">
          <h5 class="card-title">Receta:</h5>
          
        </div>
      </div> -->
    </div>
     <center>
        <div class="card-body">
          <a href="/admin/verPlantas" class="btn btn-outline-info">Regresar</a>
          <a href="/admin/EliminarPlanta/{{$planta->Id_planta}}" class="btn btn-danger">Eliminar</a>
        </div>  
   </center> 
  </div>
</div> 

@endsection
@section('js')

<script type="text/javascript">
 
    $("#id").fileinput({
        theme: "fas",
        overwriteInitial:true,
        autoReplace:true,
        initialPreviewAsData:true,
        @if($planta->imagen)
          initialPreview: [

            '{{ asset("images/".$planta->imagen) }}'
          ],

          initialPreviewConfig: [
            {type: "image", caption: "logopropueta.png"},
          ],
        @endif

        msgFileRequiered:"Ingresa una imagen",
        msgSizeTooLarge: "El archivo{name} ({Size} KB) excede el tamaño maximo permitido de {maxSize}",
        maxFileSize:1024,
        maxFileCount: 1,
        showRemove: false,
        showUpload: false,
        required: true,
        allowedFileExtensions:["jgp","png","jpeg"]

    });
</script>
<!-- archivos js dependientes de la vista -->
@endsection
