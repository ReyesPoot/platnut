@extends('admin/layout/admin')
@section('content')


    @php $urlImagen= 'images/'. $verdura->imagen; @endphp
    
<div class="container" >
  <div class="card mb-4">
    <div class="row">
      <div class="col-md-4">
          <img src="{{ asset($urlImagen)}}" class="img-fluid" alt="{{ asset($urlImagen)}}">
      </div>
      <div class="col-md-8">
          <div class="card-body">
            <h5 class="card-title">Verdura:</h5>
            <p>{{$verdura->nom_verdura}}</p>
            <h5 class="card-title">Descripcion</h5>
            <p>{{$verdura->descripcion_v}}</p>
          </div>
      </div>
    </div>

  <div class= "row">
      <div class="card-body">
        <div class= "col-md-5">
          <h5 class="card-title">Beneficio:</h5>
            <p class="card-text">{{$verdura->beneficios_v}}</p>
        </div>
      </div>
  </div>
  <div class="row">
      <div class="col-md-6">
        <div class= "card-body">
            <h5 class="card-title">Valor nutrimental:</h5> 
            <p>
            <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
             Agregar
            </a>
          </p>
              <div class="collapse" id="collapseExample">
                <div class="card card-body">
                   <form action="/admin/agregarInfoVerdura" method="POST">

                        {{csrf_field()}}
                          <div class="row no-gutters">
                            <input type="text" value="{{$verdura->Id_verdura}}" name="id" hidden>
                            <div class="col">
                                <label for="agregar">Informacion</label>
                                <input type="text" name="informacion" class= "form-control placeholder" placeholder="nombre..">
                            </div>
                            <div class="col">
                              <label for="agregar">Cantidad</label>
                              <input type="text" name="cantidad" class= "form-control placeholder" placeholder="Cantidad..">
                            </div>        
                            <div class="col">
                              <label for="agregar">Unida de medida</label>
                              <input type="text" name="unidad" class= "form-control placeholder" placeholder="Unidad..">
                            </div>  
                            <div class="form-group">
                              <input type="submit" value="Guardar" class= " btn btn-block btn-primary bnt-sm">
                            </div>   
                        </div>
                    </form>
                </div>
              </div>
              <table class="table table-bordered">
              <thead class="thead-dark">
                <tr>
                  <th>Componente</th>
                  <th>Cantidad</th>
                  <th>Unidad</th>
                </tr>
              </thead>
              <tbody>
                @foreach($verdura->informacion as $informacion)
                <tr>
                  <td>{{$informacion->informacion}}</td>
                  <td>{{$informacion->cantidad}}</td>
                  <td>{{$informacion->unidad_medida}}</td>
                 
                </tr>
                 @endforeach
              </tbody>
          </table>
        </div>
      </div>
      <div class="col-md-6">
        <div class="card-body">
          <h5 class="card-title">platillo:</h5>
          <p>
              <a class="btn btn-primary" data-toggle="collapse" href="#Receta" role="button" aria-expanded="false" aria-controls="collapseExample">
               Agregar
              </a>
            </p>
          <div class="collapse" id="Receta">
            <div class="card card-body">
               <form action="/admin/agregarPlatilloVerdura" method="POST">

                    {{csrf_field()}}

                     <input type="text" value="{{$verdura->Id_verdura}}" name="id" hidden>
                <div class="form-group">
                    <label for="agregar">Nombre del platillo</label>
                    <input type="text" name="nom_platillo" class= "form-control placeholder" placeholder="Nombre del platillo">
                </div>
                <div class="form-group">
                    <label for="agregar">Descripcion </label>
                    <input name="descripcion" class="form-control" rows="3" placeholder="escriba la descripcion del paltillo">
                </div>
                <div class="form-group">
                    <label for="agregar">Beneficios</label>
                    <input name="beneficios" class="form-control" rows="3" placeholder="escriba los beneficios del platillo">
                </div>
                <div class="form-group">
                    <input type="submit" value="Guardar" class= "btn btn-primary">
                </div>
                </form>
            </div>
          </div>
          @foreach($verdura->platillo as $platillo)
          <p>
            <a href="/admin/ConsultarPlatillos/{{$platillo->Id_platillo}}" class="btn-link">{{$platillo->nom_platillo}}</a>
          </p>
          @endforeach
        </div>
      </div>
    </div>
    <center>
        <div class="card-body">
          <a href="/admin/verVerdura" class="btn btn-outline-info">Regresar</a>
          <a href="/admin/EditarVerdura/{{$verdura->Id_verdura}}" class="btn btn-success">Actualizar</a>
           <a href="/admin/EliminarVerdura/{{$verdura->Id_verdura}}" class="btn btn-danger">Eliminar</a>
        </div>  
   </center> 
  </div>
</div> 



@endsection

@section('js')

 @endsection