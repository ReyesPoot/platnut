<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Analisis extends Model 
{

protected $fillable= ['Id_analisis','nom_analisis','tipo','fecha_created','fecha_updated'];

protected $primaryKey= 'Id_analisis';
protected $table= 'analisis_quimicos';
const CREATED_AT ='fecha_created';
const UPDATED_AT ='fecha_updated';


}

 ?>