@extends('admin/layout/admin')
@section('content')
<!-- contenido de la pagina web -->

<div class="container" >
    <div class="cold-md-6">
        <h2>Actualizar contactos</h2>
            <form action="/admin/EditarNutriologos" method="POST">

                {{csrf_field()}}

                 <div class= "file-loading">
                    <input id="id" type="file" value="{{$nutriologos->imagen}}" name="imagen" accept="imagen/*">
                </div>

                <input type="text" value="{{$nutriologos->Id_nutriologo}}" name="id" hidden>
                <div class="form-group">
                    <label for="actualizar">Nombre</label>
                    <input type="text" value="{{$nutriologos->nom_nutriologo}}" name="nutriologo" class= "form-control placeholder" placeholder="Nombre del nutriologo">
                </div>
                <div class="form-group">
                    <label for="actualizar">Direccion</label>
                    <textarea class="form-control" name="direccion" rows="3" placeholder="Direccion">{{$nutriologos->direccion}}</textarea>
                </div>
                <div class="form-group">
                    <label for="actualizar">Numero de telefono</label>
                    <textarea class="form-control" name="telefono" rows="3" placeholder="Numero de Telefono">{{$nutriologos->num_tel}}</textarea>
                </div>
                <div class="form-group">
                    <button type="submit" class= "btn btn-success">actualizar</button>
                </div>       
         </form> 
    </div>    
</div> 
@endsection
@section('js')
<!-- archivos js dependientes de la vista -->
<script type="text/javascript">

    $("#id").fileinput({
        theme: "fas",
        overwriteInitial:true,
        autoReplace:true,
        initialPreviewAsData:true,
        @if($nutriologos->imagen)
          initialPreview: [

            '{{ asset("images/".$nutriologos->imagen) }}'
          ],

          initialPreviewConfig: [
            {type: "image", caption: "logopropueta.png"},
          ],
        @endif

        msgFileRequiered:"Ingresa una imagen",
        msgSizeTooLarge: "El archivo{name} ({Size} KB) excede el tamaño maximo permitido de {maxSize}",
        maxFileSize:1024,
        maxFileCount: 1,
        showRemove: false,
        showUpload: false,
        required: true,
        allowedFileExtensions:["jgp","png","jpeg"]

    });
</script>
@endsection
