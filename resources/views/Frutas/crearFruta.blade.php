@extends('admin/layout/admin')
@section('content')
<!-- contenido de la pagina web -->
<div class="container" >
    <div class="cold-md-6">
        <h2>Agregar frutas nuevas</h2>
            <form action="/admin/crearFruta" method="POST">

                {{csrf_field()}}

                <div class= "file-loading">
                    <input id="id" type="file" name="imagen" accept="imagen/*">
                </div>
                <div class="form-group">
                    <label for="agregar frutas">Nombre de la fruta</label>
                    <input type="text" name="nom_fruta" class= "form-control placeholder" placeholder="Nombre de la  fruta">
                </div>
                <div class="form-group">
                    <label for="agregar frutas">Nombre Cientifico</label>
                    <input type="text" name="nom_cientifico" class= "form-control placeholder" placeholder="Nombre cientifico..">
                </div>

                <div class="form-group">
                    <label for="agregar frutas">Descripcion de la fruta</label>
                    <textarea class="form-control" name="descripcion" rows="3" placeholder="Escriba la descripcion de la fruta"></textarea>
                </div>

                <div class="form-group">
                    <label for="agregar frutas">Beneficios</label>
                    <textarea class="form-control" name="beneficios" rows="3" placeholder="Escriba los beneficios de la fruta"></textarea>
                </div>
                <div class="form-group">
                    
                </div>
                <div class="form-group">
                    <div class="cold-md-6">
                        <input type="submit" value="Guardar" class= "btn btn-primary">
                    </div>
                </div>
         </form> 

    </div>    
</div> 
       
@endsection
@section('js')

<script type="text/javascript">
    $("#id").fileinput({
        theme: "fas",
        msgFileRequiered:"Ingresa una imagen",
        msgSizeTooLarge: "El archivo{name} ({Size} KB) excede el tamaño maximo permitido de {maxSize}",
        maxFileSize:1024,
        maxFileCount: 1,
        showRemove: false,
        showUpload: false,
        required: true,
        allowedFileExtensions:["jgp","png","jpeg"]

    });
</script>
<!-- archivos js dependientes de la vista -->
@endsection

       

