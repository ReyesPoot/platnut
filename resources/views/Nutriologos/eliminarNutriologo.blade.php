@extends('admin/layout/admin')
@section('content')
<!-- contenido de la pagina web -->
<div class="container" >
    <div class="cold-md-6">
        <h2>Eliminar Contacto</h2>
            <form action="/admin/EliminarNutriologos" method="POST">
                
                {{csrf_field()}}
                <input type="text" value="{{$nutriologos->Id_nutriologo}}" name="id" hidden>
                <div class="form-group">
                    <label for="agregar frutas">Nombre</label>
                    <input type="text" value="{{$nutriologos->nom_nutriologo}}" name="nutriologo" class= "form-control placeholder" placeholder="Nombre">
                </div>

                <div class="form-group">
                    <label for="agregar frutas">Direccion</label>
                    <textarea class="form-control" name="descripcion" rows="3" placeholder="Direccion del contacto">{{$nutriologos->direccion}}</textarea>
                </div>

                <div class="form-group">
                    <label for="agregar frutas">Numero de telefono</label>
                    <textarea class="form-control" name="telefono" rows="3" placeholder="Telefono">{{$nutriologos->num_tel}}</textarea>
                </div>
                <div class="form-group">
                    <button type="submit" class= "btn btn-danger">Eliminar</button>
                </div>
                
                 
         </form> 
    </div>    
</div> 
       
@endsection
@section('js')
<!-- archivos js dependientes de la vista -->
@endsection
