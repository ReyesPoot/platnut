<?php

namespace App\Http\Controllers;

use App\Fruta;
use App\Verdura;
use App\Planta;
use App\Platillo;
use App\Nutriologo;
use App\Informacion;
use App\Ingrediente;
use Illuminate\Http\Request; 
use App\User;


use Illuminate\Support\Facades\Hash;

use App\Http\Controllers\Controller;

class FrutaController extends Controller
{
    //consultas para contar frutas,verduras,plantas
    public function Home()
    {
       $title="home";
       //contando frutas
       $frutas=Fruta::all();
       $tot=count($frutas);
        //contadno verduras
       $verduras=Verdura::all();
       $tol=count($verduras);

       $plantas=Planta::all();
       $total=count($plantas);

       $platillos=Platillo::all();
       $tototal=count($platillos);

       $nutriologos=Nutriologo::all();
       $toto=count($nutriologos);

        return view ('/admin/home',compact('title','tot','tol','total','tototal','toto'));
    }
    public function user()
    {
       $title="home";
       // contando frutas
       $frutas=Fruta::all();
       $tot=count($frutas);
        //contadno verduras
       $verduras=Verdura::all();
       $tol=count($verduras);

       $plantas=Planta::all();
       $total=count($plantas);

       $platillos=Platillo::all();
       $tototal=count($platillos);

       $nutriologos=Nutriologo::all();
       $toto=count($nutriologos);

        return view ('/user/home',compact('title','total','tot','tol','tototal','toto'));
    }

     public function Consultar($id)
    {
        $title="crearFruta";
       
        //consultas    
        $ingrediente=Ingrediente::where('Id_ingrediente',$id)->get();
        //Consultando la informacion nutrimental de la fruta
      
        $fruta=Fruta::where('Id_fruta',$id)->first();
        $Consulta =Fruta::find($id)->informacion()->get();
        $platillo_fruta=Fruta::find($id)->platillo()->get();

     return view('Frutas/ConsultarFruta',compact('title','fruta','platillo_fruta','Consulta','ingrediente'));

    }

    
    public function agregar()
    {
        $title="crearFruta";

        return view ('Frutas/crearFruta',compact('title'));
    }
    public function ver()
    {
        $title="crearFruta";


        $frutas= Fruta::get();
        return view ('Frutas/verFruta', compact('frutas','title'));
    }
    public function Editar($id)
    {
        $title="crearFruta";
        $fruta= Fruta::where('Id_fruta',$id)->first();
        $platillo=Platillo::where('Id_platillo',$id)->get();

        return view ('Frutas/ConsultarActualizarF', compact('title','fruta','platillo'));

    }
    public function Eliminar($id)
    {
        $title="crearFruta";
        //dd($id);
        $fruta= Fruta::where('Id_fruta',$id)->first();
        $platillo=Platillo::where('Id_platillo',$id)->get();
         return view ('Frutas/ConsultarEliminar', compact('title','fruta','platillo'));

    } 
    public function update(Request $request)
    {   

    
        $fruta= Fruta::where('Id_fruta',$request->id)->first();
        $fruta->imagen=$request->imagen;
        $fruta->nom_fruta=$request->nom_fruta;
        $fruta->descripcion_f=$request->descripcion;
        $fruta->beneficios_f=$request->beneficios;
             //  dd($fruta);  
        $fruta->save();

        return redirect('/admin/verFruta'); 

    }
    public function destroy(Request $request)
    {   
        
        $frutas= Fruta::where('Id_fruta',$request->id)->first();
        $frutas->delete();
        return redirect('/admin/verFruta');
        
    }

   
     public function store(Request $request)
    {
        // dd($_POST);

        $Guardar= new fruta;
        $Guardar->imagen=$request->imagen;
        $Guardar->nom_fruta= $request->nom_fruta;
        $Guardar->nom_cientifico= $request->nom_cientifico;
        $Guardar->descripcion_f= $request->descripcion;
        $Guardar->beneficios_f= $request->beneficios;
       
        //dd($Guardar);
        $Guardar->save();
        return redirect('/admin/verFruta'); 
    }
}