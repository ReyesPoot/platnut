@extends('admin/layout/admin')
@section('content')

@php $urlImagen= 'images/'. $nutriologo->imagen; @endphp
      <div class="container" >
       <div class="row justify-content-center">
          <div class="col-md-10">
            <div class="card mb-4">
              <div class="row no-gutters">
                <div class="col-md-3">
                  <img src="{{ asset($urlImagen)}}" class="img-fluid" alt="{{ asset($urlImagen)}}">
                </div>
                 <div class="col-md-9">
                  <div class="card-body">
                    <h5 class="card-title">Nutriologo:</h5>
                      <p>{{$nutriologo->nom_nutriologo}}</p>
                     <h5 class="card-title">Direccion</h5>
                      <p>{{$nutriologo->direccion}}</p>
                      <h5 class="card-title">Telefono:</h5>
                      <p>{{$nutriologo->num_tel}}</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
     </div>
    </div>            

@endsection

@section('js')

 @endsection