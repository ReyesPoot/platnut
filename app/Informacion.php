<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Informacion extends Model 
{

	protected $fillable= ['Id_informacion','nom_producto','cantidad','unidad_medida','fecha_created','fecha_updated'];

	protected $primaryKey= 'Id_informacion';
	protected $table= 'informacion_nutri';
	const CREATED_AT ='fecha_created';
	const UPDATED_AT ='fecha_updated';

	//Relaciones
	// public function padecidad(){
	// 	return $this->hasOne('App/Padecidad','Id_informacion','Id_padecidad');

	// }
	public function fruta(){
		return $this->belongsToMany('App\Fruta','info_fruta','Id_informacion','Id_fruta');
	 }
	 public function platillo(){
		return $this->belongsToMany('App\Platillo','info_platillo','Id_informacion','Id_platillo');
	 }
	 public function verdura(){
		return $this->belongsToMany('App\Verdura','info_verdura','Id_informacion','Id_verdura');
	 }
	 public function planta(){
		return $this->belongsToMany('App\Planta','info_planta','Id_informacion','Id_planta');
	 }

	
}

 ?>