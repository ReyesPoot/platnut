$(document).ready( function(){
  //Eliminar normatividad
  $(".container-options-normatividad").on("click", ".eliminar-normatividad", function (event) {
    var id = $(this).data("idnormatividad");
    $("#enlace-eliminar-normatividad").attr('href', '/admin/normatividad/eliminar/' + id);
  });

  //Eliminar noticia
  $(".eliminar-noticia").on("click", function () {
    var id = $(this).data("idnoticia");
    $("#enlace-eliminar-noticia").attr('href', '/admin/noticias/eliminar/' + id);
  });

  //Eliminar liga
  $(".eliminar-liga").on("click", function () {
    var id = $(this).data("idliga");
    $("#enlace-eliminar-liga").attr('href', '/admin/ligas-interes/eliminar/' + id);
  });
});


//para seccion normatividad
var erroresValidacionRequeridosNorma = new Array();
var erroresValidacionRedundantesNorma = new Array();
var erroresValidacionNormaEditar = new Array();
var valorRadioButtonChecked;

//seccion noticias
var erroresValidacionNoticias = new Array();

//seccion ligas
var erroresValidacionLigas = new Array();

/***************************************
SECCION NORMATIVIDAD
****************************************/
/************************
agregar normatividad
************************/
$("#form-agregar-normas").on('submit', function(evt){
  if(!validarCamposRequeridosNorma()) {
    evt.preventDefault();
    return false;
  }

  if (!camposRedundantes()) {
    evt.preventDefault();
    return false;
  }
});

//validar campos requeridos en nueva normatividad
function validarCamposRequeridosNorma() {
  erroresValidacionRequeridosNorma = [];
  //tipo text
  var nombre = $("#nombre").val();
  var urlNorma = $("#url-norma").val();
  
  //de tipos file
  imagen = $(".portada-norma");
  nombreImagen = $(imagen).val();
  extensionImagen = nombreImagen.substring(nombreImagen.lastIndexOf(".")).toLowerCase();
  norma = $(".documento-norma");
  nombreNorma = $(norma).val();
  extensionNorma = nombreNorma.substring(nombreNorma.lastIndexOf(".")).toLowerCase();
  //revisando campos requeridos
  if(nombre == "") {
    erroresValidacionRequeridosNorma.push("El campo título de la normatividad no puede estar vacío.");
  }

  if((imagen).get(0).files[0] == undefined) {
    erroresValidacionRequeridosNorma.push("El campo portada de la normatividad no puede estar vacío, adjunte una portada por favor.");
  }

  if((imagen).get(0).files[0] !== undefined) {
    var pesoOrigen = $(imagen).get(0).files[0].size;
    var pesoKB = Math.round(pesoOrigen / 1024);
    extensioFotoAllowed = [".jpg", ".jpeg", ".png"];
    if(!extensioFotoAllowed.some(function(x) { return x==extensionImagen; })){
      erroresValidacionRequeridosNorma.push("Sólo se admite portadas en formato .jpg, .jpeg o .png.");
    }
    if (pesoKB > 1000) {
      var pesoMb = Math.round(pesoKB / 1024);
      if (pesoMb > 1) {
        erroresValidacionRequeridosNorma.push("El peso de la portada excede la cantidad admitida de 1 MB.");
      } 
    }
  }

  if((norma).get(0).files[0] !== undefined) {
    var pesoOrigen = $(norma).get(0).files[0].size;
    var pesoKB = Math.round(pesoOrigen / 1024);
    extensioFotoAllowed = [".pdf"];
    if(!extensioFotoAllowed.some(function(x) { return x==extensionNorma; })){
      erroresValidacionRequeridosNorma.push("Sólo se admite documentos en formato .pdf.");
    }
    if (pesoKB > 1000) {
      var pesoMb = Math.round(pesoKB / 1024);
      if (pesoMb > 3) {
        erroresValidacionRequeridosNorma.push("El peso del documento excede la cantidad admitida de 3 MB.");
      } 
    }
  }

  if((norma).get(0).files[0] == undefined && urlNorma == "" ) {
    erroresValidacionRequeridosNorma.push("No es posible guardar los datos porque no se ha adjuntado una normatividad o ingresado un enlace externo.");
  }

  if (erroresValidacionRequeridosNorma.length > 0) {
    window.scrollTo(0, 0);
    var body = "";
    $.each(erroresValidacionRequeridosNorma, function(key, value) {
      body = body + "<div class='alert alert-danger alert-dismissible fade show' role='alert'>"+
      "<p>"+ value +"</p>"+
      "<button type='button' class='close' data-dismiss='alert' aria-label='Close'>"+
      "<span aria-hidden='true'>&times;</span>"+
      "</button>"+
      "</div>";
    });
    $(".mensajes-error-validacion-front").html(body);
    return false;
  } else{
    return true;
  }
}

function camposRedundantes() {
  erroresValidacionRedundantesNorma = [];
  var urlNorma = $("#url-norma").val();
  norma = $(".documento-norma");

  if((norma).get(0).files[0] !== undefined && urlNorma !== "" ) {
    erroresValidacionRedundantesNorma.push("No es posible guardar los datos porque existen campos redundantes. No se permite adjuntar la normatividad y al mismo tiempo ingresar un enlace externo de la misma. No se admiten ambos.");
  }

  if (erroresValidacionRedundantesNorma.length > 0) {
    window.scrollTo(0, 0);
    var body = "";
    $.each(erroresValidacionRedundantesNorma, function(key, value) {
      body = body + "<div class='alert alert-danger alert-dismissible fade show' role='alert'>"+
      "<p>"+ value +"</p>"+
      "<button type='button' class='close' data-dismiss='alert' aria-label='Close'>"+
      "<span aria-hidden='true'>&times;</span>"+
      "</button>"+
      "</div>";
    });
    $(".mensajes-error-validacion-front").html(body);
    return false;
  } else{
    return true;
  }
}

function deshabilitarUrl() {
  norma = $(".documento-norma");
  if((norma).get(0).files[0] !== undefined) {
    alert("si entra");
    $("#url-norma").prop("disabled", true);
  } else {
    alert("no entra");
  }
}

/*******************************
editar normatividad
*******************************/
/*function disabledUrlExtra(id) {
  var modalEditar = $("#modaleditar" + id);
  normaFile = $(modalEditar).find("#file-norma-editar-norma"+id);
  
  normaFile.on('change', function() {
    if ((normaFile).get(0).files[0] !== undefined) {
      $(modalEditar).find("#url-documento").prop('disabled', true);
    } else {
      $(modalEditar).find("#url-documento").prop('disabled', false);
    }
  });*/
$(".habilitar-edicion-norma").on('click', function() {
  $('.container-opciones-edicion-norma').fadeTo();
});

$(".onlyfile").on("change", function() {
    valorRadioButtonChecked = $(this).val();
    bodyFileNorma = "<div class='col-md-12 mt-2'>" +
                        "<div>" +
                          "<input id='file-archivo-editar' name='norma_file' type='file' class='file' data-overwrite-initial='false'>" +
                        "</div>" +
                      "<h6>SELECCIONE EL NUEVO DOCUMENTO</h6>" +
                      "<p>Elija el documento que desea subir</p>" +
                    "</div>";
  $(this).parents(".edit-norma-form").find(".container-url-norma").html("");
  $(this).parents(".edit-norma-form").find(".container-file-norma").html(bodyFileNorma);
});


$(".onlyurl").on("change", function() {

    bodyFileUrl = "<div class='col-md-12 mt-2'>" +
                  "<p>En caso de sólo contar con el enlace, puede escribirlo en el siguiente apartado.</p>" +
                  "<input type='text' class='form-control' id='url-documento' name='url_norma' placeholder='URL'>" +
                "</div>";
  $(this).parents(".edit-norma-form").find(".container-file-norma").html("");
  $(this).parents(".edit-norma-form").find(".container-url-norma").html(bodyFileUrl);
});


//submit formulario editar normatividad
$(".edit-norma-form").on('submit', function(evt){
  erroresValidacionNormaEditar = [];
  //definiendo los datos a guardar
  id_norma = $(this).find("#id-norma").val();
  nombreNorma = $(this).find("#nombre-norma").val();
  urlNorma = $(this).find("#url-documento").val();
  portadaFile = $(this).find("#file-portada-editar-norma");
  normaFile = $(this).find("#file-norma-editar-norma");

  //si existe portada adjunta se agrega
  if ((portadaFile).get(0).files[0] !== undefined) {
    var pesoOrigen = (portadaFile).get(0).files[0].size;
    var pesoKB = Math.round(pesoOrigen / 1024);
    nombrePortada = $(portadaFile).val();
    extensionFoto = nombrePortada.substring(nombrePortada.lastIndexOf(".")).toLowerCase();
    extensioFotoAllowed = [".png", ".jpg", ".jpeg"];
    if(!extensioFotoAllowed.some(function(x) { return x==extensionFoto; })){
      erroresValidacionNormaEditar.push("La portada admite únicamente formatos .jpg, .jpeg o .png.");
    }
    if (pesoKB > 1000) {
      var pesoMb = Math.round(pesoKB / 1024);
      if (pesoMb > 1) {
        erroresValidacionNormaEditar.push("El peso de la portada excede la cantidad admitida de 1 MB.");
      } 
    }
  }

  //si adjuntar archivo esta habilitado revisar que no se coloque una url
  if ((normaFile).get(0).files[0] !== undefined) {
    if(urlNorma !== undefined) {
      erroresValidacionNormaEditar.push("No es posible guardar los datos, debido a que usted está adjuntado una normatividad y al mismo tiempo está proporcionando una url externa del mismo. Sólo es posible hacer una acción a la vez.");
    } else {
      var pesoOrigen = (normaFile).get(0).files[0].size;
      var pesoKB = Math.round(pesoOrigen / 1024);
      nombreFile = $(normaFile).val();
      extensionFile = nombreFile.substring(nombreFile.lastIndexOf(".")).toLowerCase();
      extensioFotoAllowed = [".pdf"];
      if(!extensioFotoAllowed.some(function(x) { return x==extensionFile; })){
        erroresValidacionNormaEditar.push("El documento admite únicamente formato .pdf.");
      }
      if (pesoKB > 1000) {
        var pesoMb = Math.round(pesoKB / 1024);
        if (pesoMb > 3) {
          erroresValidacionNormaEditar.push("El peso del documento excede la cantidad admitida de 3 MB.");
        }
      }
    }
  }

  if (erroresValidacionNormaEditar.length > 0) {
    var body = "";
    $.each(erroresValidacionNormaEditar, function(key, value) {
      body = body + "<div class='alert alert-danger alert-dismissible fade show' role='alert'>"+
      "<p>"+ value +"</p>"+
      "<button type='button' class='close' data-dismiss='alert' aria-label='Close'>"+
      "<span aria-hidden='true'>&times;</span>"+
      "</button>"+
      "</div>";
    });
    $(".error-mensajes-modal" + id_norma).html(body);
    evt.preventDefault();
    return false;
  }
});

//busqueda de normatividad
$("#input-buscar-norma").on('keyup', function (e) {
    if (e.keyCode == 13) {
        var palabraClaveInsertada = $(this).val();
        if (palabraClaveInsertada !== "") {
          var request = $.ajax({
          url: "/admin/normatividad/buscar/" + palabraClaveInsertada,
          type: "GET",
          //processData: false, // tell jQuery not to process the data
          //contentType: false,
          success: function(result){
            if (result.normas !== undefined ) {
              var total = result.normas.length;
              if(total > 0){
                mostrarNormas(result.normas);
              }
            } else if (result.cero !== undefined ) {
              $("#container-tabla-normas").empty();
              $("#container-tabla-normas").html("<p>Sin resultados de búsqueda obtenidos. Intente con otra palabra.</p>")
            }
        }
          });
        }
    }
});

//mostras resultados de busqueda normatividad
function mostrarNormas(response) {
  bodyTabla = "";
  $("#container-tabla-normas").html("");
  headerTabla = "<table class='table table-sm text-center border-table'>" +
  "<thead>" +
  "<tr>" +
  "<th scope='col' class='text-center titletabla'>No</th>" +
  "<th scope='col' class='text-left titletabla'>Título</th>" +
  "<th scope='col' class='text-left titletabla'>Formato</th>" +
  "<th scope='col' class='text-left titletabla'>Peso</th>" +
  "<th scope='col' class='text-left titletabla opciones'>Opciones</th>" +
  "</tr>" +
  "</thead>" +
  "<tbody>";
  footerTabla = "</tbody></table>";
  var i = 1;
  $.each(response, function(key, value) {
      bodyTabla = bodyTabla + "<tr>" +
      "<th scope='row' class='text-center'>" + i +"</th>" +
      "<td class='text-left'>" + value.nombre + "</td>" +
      "<td class='text-left'>" + value.formato + "</td>" +
      "<td class='text-left'>" + value.peso + "</td>" +
      "<td class='text-left' id='container-options-normatividad'>" +
      "<a href='" + value.url_documento + "' target='_blank'>" +
      "Ver" +
      "</a>"+
      "|"+
      "<a href='#' data-toggle='modal' data-target='#modaleditar" + value.id_normatividad +"'>" +
      "Editar"+
      "</a>" + "|" +
      "<a href='#' data-toggle='modal' class='eliminar-normatividad' data-target='.modaleliminar' data-idnormatividad=" + value.id_normatividad + ">" +
      "Eliminar"+
      "</a>"+
      "</td>"+
      "</tr>";
      i++;
  });
  bodyTabla = headerTabla + bodyTabla + footerTabla;
  $("#container-tabla-normas").append(bodyTabla);
}


/*******************************************************
SECCION LIGAS INTERES
********************************************************/
/*********************************
agregar ligas interes con validacion en front
**********************************/
$("#form-agregar-ligas").on('submit', function(evt){
  //limpiamos array de errores de validacion
  erroresValidacionLigas = [];
  portadaLiga = $("#portada-liga");
  nombrePortadaLiga = $(portadaLiga).val();
  extensionPortadaLiga = nombrePortadaLiga.substring(nombrePortadaLiga.lastIndexOf(".")).toLowerCase();

  //incluye formmato y peso de 1 megas
  if((portadaLiga).get(0).files[0] !== undefined) {
    var pesoOrigen = (portadaLiga).get(0).files[0].size;
    var pesoKB = Math.round(pesoOrigen / 1024);
    extensioFotoAllowed = [".jpg", ".jpeg", ".png"];
    if(!extensioFotoAllowed.some(function(x) { return x==extensionPortadaLiga; })){
      erroresValidacionLigas.push("Sólo se admite portadas en formato .jpg, .jpeg o .png.");
    }
    if (pesoKB > 1000) {
      var pesoMb = Math.round(pesoKB / 1024);
      if (pesoMb > 1) {
        erroresValidacionLigas.push("El peso de la imagen excede la cantidad admitida de 1 MB.");
      }
    }
  }
  //revisamos existencia de errores
  if(erroresValidacionLigas.length > 0) {
    window.scrollTo(0, 0);
    var body = "";
    $.each(erroresValidacionLigas, function(key, value) {
      body = body + "<div class='alert alert-danger alert-dismissible fade show' role='alert'>"+
      "<p>"+ value +"</p>"+
      "<button type='button' class='close' data-dismiss='alert' aria-label='Close'>"+
      "<span aria-hidden='true'>&times;</span>"+
      "</button>"+
      "</div>";
    });
    $(".mensajes-error-validacion-front").html(body);
    evt.preventDefault();
    return false;
  }
 });

/********************
editar ligas interes submit formulario
********************/
$(".form-editar-liga").on('submit', function(evt) {
    //definiendo los datos a guardar
    erroresValidacionLigas = [];
    id_liga = $(this).find("#id-liga").val();
    nombreLiga = $(this).find("#nombre").val();
    urlLiga = $(this).find("#link").val();
    imagenLiga = $(this).find("#url-imagen");
    nombrePortadaLiga = $(imagenLiga).val();
    extensionPortadaLiga = nombrePortadaLiga.substring(nombrePortadaLiga.lastIndexOf(".")).toLowerCase();

    //si existe portada adjunta se agrega
    if ((imagenLiga).get(0).files[0] !== undefined) {
      var pesoOrigen = (imagenLiga).get(0).files[0].size;
      var pesoKB = Math.round(pesoOrigen / 1024);
      extensioFotoAllowed = [".jpg", ".jpeg", ".png"];
      if(!extensioFotoAllowed.some(function(x) { return x==extensionPortadaLiga; })){
        erroresValidacionLigas.push("Sólo se admite portadas en formato .jpg, .jpeg o .png.");
      }
      if (pesoKB > 1000) {
        var pesoMb = Math.round(pesoKB / 1024);
        if (pesoMb > 1) {
          erroresValidacionLigas.push("El peso de la imagen excede la cantidad admitida de 1 MB.");
        } 
      }
    }

    if (erroresValidacionLigas.length > 0) {
      var body = "";
      $.each(erroresValidacionLigas, function(key, value) {
        body = body + "<div class='alert alert-danger alert-dismissible fade show' role='alert'>"+
        "<p>"+ value +"</p>"+
        "<button type='button' class='close' data-dismiss='alert' aria-label='Close'>"+
        "<span aria-hidden='true'>&times;</span>"+
        "</button>"+
        "</div>";
      });
      $(".error-mensajes-modal" + id_liga).html(body);
      evt.preventDefault();
      return false;
    }
});

/***************************************
mostrar resultados en ligas interes
***************************************/
function mostrarLigas(response) {
  bodyTabla = "";
  $("#container-tabla-ligas").html("");
  headerTabla = "<table class='table table-sm text-center border-table'>" +
  "<thead>" +
  "<tr>" +
  "<th scope='col' class='text-center titletabla'>ID</th>" +
  "<th scope='col' class='text-left titletabla'>Título</th>" +
  "<th scope='col' class='text-left titletabla'>URL</th>" +
  "<th scope='col' class='text-left titletabla opciones'>Opciones</th>" +
  "</tr>" +
  "</thead>" +
  "<tbody>";
  footerTabla = "</tbody></table>";
  var i = 1;
  $.each(response, function(key, value) {
      bodyTabla = bodyTabla + "<tr>" +
      "<th scope='row' class='text-center'>" + i +"</th>" +
      "<td class='text-left'>" + value.nombre + "</td>" +
      "<td class='text-left'>" + value.link + "</td>" +
      "<td class='text-left'>" +
      "<a href='#' data-toggle='modal' data-target='#modaleditar"+value.id_liga+"'>" +
      "Editar"+
      "</a>" +
      "<a href='#' data-toggle='modal' data-target='#modaleliminar" + value.id_liga + "'>" +
      "Borrar"+
      "</a>"+
      "</td>"+
      "</tr>";
      i++;
  });
  bodyTabla = headerTabla + bodyTabla + footerTabla;
  $("#container-tabla-ligas").append(bodyTabla);
}

/*******************************
SECCION NOTICIAS
*******************************/

/*****************************
agregar noticias
******************************/
  $("#tipo-noticias-convocatorias").on(('change'), function(){
    if ($(this).is(':checked')) {
      $("#container-fecha-noticia").removeClass("d-none");
    } else {
      $("#container-fecha-noticia").addClass("d-none");
    }
  });

  $("#tipo-noticias-investigador").on(('change'), function(){
    if ($(this).is(':checked')) {
      $("#container-fecha-noticia").addClass("d-none");
    }
  });

  $("#tipo-noticias-ciencia").on(('change'), function(){
    if ($(this).is(':checked')) {
      $("#container-fecha-noticia").addClass("d-none");
    }
  });

//submit del formulario de agregar noticias
$("#form-agregar-noticia").on('submit', function(evt){
    //reset errores array
    erroresValidacionNoticias = [];
    //revisamos si es una noticia tipo convocatoria y validamos las fechas en su caso.
    if (esConvocatoria()) {
      var fechaInicio = $("#fecha-inicio").val();
      var fechaFin = $("#fecha-fin").val();
      if (!compararFecha(new Date(fechaInicio), new Date(fechaFin))) {
        erroresValidacionNoticias.push("El valor de la fecha de apertura de la convocatoria no puede ser superior que la fecha de cierre.");
      }
    }

    //llamamos validadores
    validarCamposRequeridosNoticias();

    //revisamos existencia de errores
    if (erroresValidacionNoticias.length > 0) {
      window.scrollTo(0, 0);
      var body = "";
      $.each(erroresValidacionNoticias, function(key, value) {
        body = body + "<div class='alert alert-danger alert-dismissible fade show' role='alert'>"+
        "<p>"+ value +"</p>"+
        "<button type='button' class='close' data-dismiss='alert' aria-label='Close'>"+
        "<span aria-hidden='true'>&times;</span>"+
        "</button>"+
        "</div>";
      });
      $(".mensajes-error-validacion-front").html(body);
      evt.preventDefault();
      return false;
    }
 });

function esConvocatoria() {
  if ($("#tipo-noticias-convocatorias").is(':checked')) {
    return true;
  } else {
    return false;
  }
}

function validarCamposRequeridosNoticias() {
  portadaNoticia = $(".url-portada-noticia");
  nombrePortadaNoticia = $(portadaNoticia).val();
  extensionPortadaNoticia = nombrePortadaNoticia.substring(nombrePortadaNoticia.lastIndexOf(".")).toLowerCase();
  urlNoticia = $("#link").val();
  if((portadaNoticia).get(0).files[0] !== undefined) {
    var pesoOrigen = (portadaNoticia).get(0).files[0].size;
    var pesoKB = Math.round(pesoOrigen / 1024);
    extensioFotoAllowed = [".jpg", ".jpeg", ".png"];
    if(!extensioFotoAllowed.some(function(x) { return x==extensionPortadaNoticia; })){
      erroresValidacionNoticias.push("Sólo se admite portadas en formato .jpg, .jpeg o .png.");
    }
    if (pesoKB > 1000) {
      var pesoMb = Math.round(pesoKB / 1024);
      if (pesoMb > 1) {
        erroresValidacionNoticias.push("El peso de la portada de la noticia excede la cantidad admitida de 1 MB.");
      }
    }
  } else {
    erroresValidacionNoticias.push("La portada de la noticia es un campo requerido.");
  }
}

/*******************************
editar noticias
*******************************/
function mostrarCampoFecha(valor) {
   if ($("#tipo-convocatoria" + valor).is(':checked')) {
      $("#container-fecha-noticia"+valor).removeClass("d-none");
   } else {
      $("#container-fecha-noticia"+valor).addClass("d-none");
   }
}

function ocultarCampoFecha(valor) {
    alert("Si la noticia está guardado como convocatoria, al cambiar el tipo las fechas de apertura y cierre se perderán.");
    $("#container-fecha-noticia" + valor).addClass("d-none");
}

/**************************************
submit form editar noticias
***************************************/
$(".edit-noticia-form").on('submit', function(evt) {
  //limpiamos array de errores
  erroresValidacionNoticias = [];
  //definiendo los datos a guardar
  id_noticia = $(this).find("#id-noticia").val();
  titulo = $(this).find("#titulo-noticia").val();
  tipo_noticia = $(this).find(".tipo-noticia").val();
  link = $(this).find("#url-noticia").val();
  imagen = $(this).find("#url-portada");
  nombreImagen = $(imagen).val();

  //para Convocatorias
  fechaInicio = $(this).find(".fecha-inicio").val();
  fechaFin = $(this).find(".fecha-fin").val();

  //si existe portada adjunta se agrega
  if ((imagen).get(0).files[0] !== undefined) {
    var pesoOrigen = (imagen).get(0).files[0].size;
    var pesoKB = Math.round(pesoOrigen / 1024);
    extensionFoto = nombreImagen.substring(nombreImagen.lastIndexOf(".")).toLowerCase();
    extensioFotoAllowed = [".png", ".jpg", ".jpeg"];
    if(!extensioFotoAllowed.some(function(x) { return x==extensionFoto; })){
      erroresValidacionNoticias.push("La portada admite únicamente formatos .jpg, .jpeg o .png.");
    }
    if (pesoKB > 1000) {
      var pesoMb = Math.round(pesoKB / 1024);
      if (pesoMb > 1) {
        erroresValidacionNoticias.push("El peso de la imagen excede la cantidad admitida de 1 MB.");
      }
    }
  }

  //si noticia es convocatoria revisar la seleccion de fechas
  if ($(this).find(".tipo-convocatoria").is(':checked')) {
    if (fechaInicio == "" || fechaFin == "") {
      erroresValidacionNoticias.push("La fecha de apertura y fecha de cierre son campos requeridos.");
    } else {
      if (!compararFecha(new Date(fechaInicio), new Date(fechaFin))) {
        erroresValidacionNoticias.push("El valor de la fecha de apertura de la convocatoria no puede ser superior que la fecha de cierre.");
      }
    }
  }
  
  if (erroresValidacionNoticias.length > 0) {
    var body = "";
    $.each(erroresValidacionNoticias, function(key, value) {
      body = body + "<div class='alert alert-danger alert-dismissible fade show' role='alert'>"+
      "<p>"+ value +"</p>"+
      "<button type='button' class='close' data-dismiss='alert' aria-label='Close'>"+
      "<span aria-hidden='true'>&times;</span>"+
      "</button>"+
      "</div>";
    });
    $(".error-mensajes-modal" + id_noticia).html(body);
    evt.preventDefault();
    return false;
  }
});


/*********************************************
FUNCION GENERAL PARA MOSTRAR MENSAJES DE ERROR
**********************************************/
function mostrarErroresAjax(response, identificador) {
  $(".error-mensajes").empty();
  $.each(response, function(key, value) {
    $(".error-mensajes-modal" + identificador).html("<div class='alert alert-danger alert-dismissible fade show' role='alert'>"+
        "<p>"+ value +"</p>"+
        "<button type='button' class='close' data-dismiss='alert' aria-label='Close'>"+
          "<span aria-hidden='true'>&times;</span>"+
        "</button>"+
    "</div>");
  });
}

/*****************************************
FUNCIONES GENERALES
*****************************************/
function validarUrl(url) {
  var correctUrl = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/;
  if (correctUrl.test(url)) {
    return true;
  } else {
    return false;
  }
}

//comparar fechas
function compararFecha(fechaInicio, fechaFin) {
  if (fechaInicio <= fechaFin) {
    return true;
  } else {
    return false;
  }
}

//compara solo para noticias
function compararFechaEnNoticias(fechaInicio, fechaFin) {
  objFechaInicio = new Date(fechaInicio);
  objFechaFin = new Date(fechaFin);
  if (objFechaInicio < objFechaFin) {
    return true;
  } else {
    return false;
  }
}

function mostrarErroresValidacionModal(errores, identificador) {
  var cantidadErrores = errores.length;
  var body = "";
  $('.error-mensajes-modal' + identificador).html();
  if (cantidadErrores > 0) {
    $(errores).each(function(key, value) {
      body = body + "<div class='alert alert-danger alert-dismissible fade show' role='alert'>"+
        "<p>" + value + "</p>"+
        "<button type='button' class='close' data-dismiss='alert' aria-label='Close'>" +
          "<span aria-hidden='true'>&times;</span>" +
        "</button>" +
        "</div>";
      $('.error-mensajes-modal' + identificador).html(body);
    });
  } else {
    return false;
  }
}
