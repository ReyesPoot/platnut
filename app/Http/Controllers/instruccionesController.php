<?php

namespace App\Http\Controllers;

use App\Platillo;
use App\Informacion;
use App\Ingrediente;
use App\Procedimiento;
use App\instrucciones;

use Illuminate\Http\Request; 

use App\Http\Controllers\Controller;

class instruccionesController extends Controller
{

   
//----------------------------administrador de ingredientes-------------------

    //funcion para agregar Ingredientes
    public function Editar($id_instru_platillo,$id)
    {
        
        $instrucciones= instrucciones::where('Id_instrucciones',$id)->first();
        $id_platillo = $id_instru_platillo;
        return view ('instrucciones/actualizarInstrucciones', compact('id_platillo','instrucciones'));
       
    }

     public function Actualizar(Request $request)

     {
      
        $instrucciones= instrucciones::where('Id_instrucciones',$request->id)->first();
       
        $instrucciones->instrucciones= $request->instrucciones;
        $instrucciones->save();
        $info=Platillo::find($request->id);
        return redirect("/admin/ConsultarActualizar/$request->id_platillo");

     }

     public function EliminarInstrucciones($id,$id_platillo)
     {

        $instrucciones = instrucciones::find($id);

        $instrucciones->delete();

         return redirect('/admin/ConsultarEliminar/'. $id_platillo);

     }

     public function GuardarInstrucciones(Request $request)
     {
        $Guardar= new instrucciones;
       
        $Guardar->instrucciones= $request->instrucciones;
       
        $Guardar->save();
        //obteniendo el id del ingrediente
        $id_info=$Guardar->Id_instrucciones;

        $info=platillo::find($request->id);
        $info->instrucciones()->attach($id_info);

        return redirect('/admin/ConsultarPlatillos/'.$request->id); 

     } 
      

}