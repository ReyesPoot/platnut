-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 25-06-2019 a las 03:45:50
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `nutriapp`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE `administrador` (
  `Id_admin` int(11) NOT NULL,
  `nom_usuario` varchar(15) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `fecha_created` datetime NOT NULL,
  `fecha_updated` datetime NOT NULL,
  `remember_token` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`Id_admin`, `nom_usuario`, `password`, `fecha_created`, `fecha_updated`, `remember_token`) VALUES
(1, 'Reyes', '123456', '2019-04-09 18:00:41', '2019-05-15 00:00:00', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `analisis_quimicos`
--

CREATE TABLE `analisis_quimicos` (
  `Id_analisis` int(11) NOT NULL,
  `nom_analisis` varchar(40) DEFAULT NULL,
  `tipo` varchar(20) DEFAULT NULL,
  `fecha_created` datetime NOT NULL,
  `fecha_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fruta`
--

CREATE TABLE `fruta` (
  `Id_fruta` int(11) NOT NULL,
  `nom_fruta` varchar(255) DEFAULT NULL,
  `descripcion_f` varchar(255) DEFAULT NULL,
  `beneficios_f` varchar(255) DEFAULT NULL,
  `fecha_created` datetime NOT NULL,
  `fecha_updated` datetime NOT NULL,
  `imagen` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fruta`
--

INSERT INTO `fruta` (`Id_fruta`, `nom_fruta`, `descripcion_f`, `beneficios_f`, `fecha_created`, `fecha_updated`, `imagen`) VALUES
(1, 'Platano', 'Es una planta que tiene hojas la cual es la que se utiliza.', 'Tiene nutrientes naturales', '2019-04-09 18:00:41', '2019-06-18 15:44:49', 'logopropuesta.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `informacion_nutri`
--

CREATE TABLE `informacion_nutri` (
  `Id_informacion` int(11) NOT NULL,
  `informacion` varchar(255) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `unidad_medida` varchar(20) DEFAULT NULL,
  `fecha_created` datetime NOT NULL,
  `fecha_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `informacion_nutri`
--

INSERT INTO `informacion_nutri` (`Id_informacion`, `informacion`, `cantidad`, `unidad_medida`, `fecha_created`, `fecha_updated`) VALUES
(1, 'Proteinas', 3, 'gramos', '2019-04-09 18:00:41', '2019-06-18 15:49:28'),
(2, 'carbohidratos', 6, 'gramos', '2019-04-09 18:00:41', '2019-06-18 15:20:55'),
(3, 'Lipidos', 3, 'gr', '2019-06-14 16:11:17', '2019-06-14 16:13:57'),
(9, 'Energia', 1, 'gramos', '2019-06-14 16:30:06', '2019-06-14 16:30:06'),
(18, 'sodio', 2, 'gramos', '2019-06-14 16:43:52', '2019-06-14 16:43:52'),
(33, 'Energias', 4, 'gramos', '2019-06-14 17:19:11', '2019-06-18 15:41:16'),
(38, 'sodio', 1, 'gramos', '2019-06-14 17:28:08', '2019-06-14 17:28:08'),
(39, 'Energias', 2, 'gramos', '2019-06-14 17:30:32', '2019-06-14 17:30:32'),
(40, 'Energias', 2, 'gramos', '2019-06-14 17:32:12', '2019-06-14 17:32:12'),
(41, 'fibra', 4, 'gramos', '2019-06-14 17:33:38', '2019-06-14 17:33:38'),
(42, 'fibra', 4, 'gramos', '2019-06-18 18:07:31', '2019-06-18 18:07:31'),
(43, 'sodio', 1, 'gramos', '2019-06-18 18:09:24', '2019-06-18 18:09:24'),
(44, 'sodio', 1, 'gramos', '2019-06-18 18:11:01', '2019-06-18 18:11:01'),
(45, 'sodio', 1, 'gramos', '2019-06-18 18:11:16', '2019-06-18 18:11:16'),
(46, 'sodio', 2, 'gramos', '2019-06-18 18:14:34', '2019-06-18 18:14:34'),
(47, 'sodio', 2, 'gramos', '2019-06-18 18:15:31', '2019-06-18 18:15:31'),
(48, 'sodio', 2, 'gramos', '2019-06-18 18:17:20', '2019-06-18 18:17:20'),
(49, 'sodio', 9, 'gramos', '2019-06-18 18:18:36', '2019-06-18 18:18:36'),
(50, 'Energia', 2, 'gramos', '2019-06-18 18:21:41', '2019-06-18 18:21:41'),
(51, 'Energia', 4, 'gramos', '2019-06-18 18:22:24', '2019-06-18 18:22:24'),
(52, 'Energia', 4, 'gramos', '2019-06-18 18:22:45', '2019-06-18 18:22:45'),
(53, 'fibra', 1, 'gramos', '2019-06-18 18:32:28', '2019-06-18 18:32:28'),
(54, 'fibra', 1, 'gramos', '2019-06-18 18:33:10', '2019-06-18 18:33:10'),
(55, 'fibra', 1, 'gramos', '2019-06-18 18:33:48', '2019-06-18 18:33:48'),
(56, 'fibra', 1, 'gramos', '2019-06-18 18:34:32', '2019-06-18 18:34:32'),
(57, 'LIpidos', 2, 'gramos', '2019-06-18 18:35:14', '2019-06-18 18:35:14'),
(58, 'Lipidos', 2, 'gramos', '2019-06-18 18:35:48', '2019-06-18 18:35:48'),
(59, 'calcio', 3, 'gramos', '2019-06-19 15:02:00', '2019-06-19 15:02:00'),
(60, 'xx', 2, 'xx', '2019-06-21 15:24:56', '2019-06-21 15:24:56'),
(61, 'xx', 2, 'xx', '2019-06-21 15:25:41', '2019-06-21 15:25:41'),
(62, 'xxx', 2, 'xxx', '2019-06-21 15:26:52', '2019-06-21 15:26:52'),
(63, 'xx1', 1, 'x', '2019-06-21 15:29:33', '2019-06-21 15:29:33');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `info_fruta`
--

CREATE TABLE `info_fruta` (
  `Id_info_fruta` int(11) NOT NULL,
  `Id_fruta` int(11) NOT NULL,
  `Id_informacion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `info_fruta`
--

INSERT INTO `info_fruta` (`Id_info_fruta`, `Id_fruta`, `Id_informacion`) VALUES
(1, 1, 2),
(3, 1, 3),
(4, 1, 9),
(5, 1, 18),
(6, 1, 42),
(7, 1, 59),
(8, 1, 60),
(9, 1, 61);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `info_planta`
--

CREATE TABLE `info_planta` (
  `Id_info_planta` int(11) NOT NULL,
  `Id_planta` int(11) NOT NULL,
  `Id_informacion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `info_planta`
--

INSERT INTO `info_planta` (`Id_info_planta`, `Id_planta`, `Id_informacion`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 56),
(4, 1, 63);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `info_platillo`
--

CREATE TABLE `info_platillo` (
  `Id_info_platillo` int(11) NOT NULL,
  `Id_platillo` int(11) NOT NULL,
  `Id_informacion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `info_platillo`
--

INSERT INTO `info_platillo` (`Id_info_platillo`, `Id_platillo`, `Id_informacion`) VALUES
(1, 1, 1),
(2, 1, 38),
(3, 1, 39),
(4, 1, 40),
(5, 1, 41),
(6, 1, 48),
(7, 1, 49),
(8, 1, 52),
(9, 1, 57),
(10, 1, 58);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `info_verdura`
--

CREATE TABLE `info_verdura` (
  `Id_info_verdura` int(11) NOT NULL,
  `Id_verdura` int(11) NOT NULL,
  `Id_informacion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `info_verdura`
--

INSERT INTO `info_verdura` (`Id_info_verdura`, `Id_verdura`, `Id_informacion`) VALUES
(1, 1, 2),
(2, 1, 1),
(3, 1, 33),
(4, 1, 45),
(5, 1, 62);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ingredientes`
--

CREATE TABLE `ingredientes` (
  `Id_ingrediente` int(11) NOT NULL,
  `ingrediente` varchar(255) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `unidad_medida` varchar(30) NOT NULL,
  `fecha_created` datetime NOT NULL,
  `fecha_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ingredientes`
--

INSERT INTO `ingredientes` (`Id_ingrediente`, `ingrediente`, `cantidad`, `unidad_medida`, `fecha_created`, `fecha_updated`) VALUES
(1, 'leche', 2, 'Litros', '2019-06-12 04:49:40', '2019-06-12 04:49:40'),
(2, 'agua', 2, 'Litros', '2019-04-09 18:00:41', '2019-05-15 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ingre_platillo`
--

CREATE TABLE `ingre_platillo` (
  `Id_ingre_platillo` int(11) NOT NULL,
  `Id_platillo` int(11) NOT NULL,
  `Id_ingrediente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ingre_platillo`
--

INSERT INTO `ingre_platillo` (`Id_ingre_platillo`, `Id_platillo`, `Id_ingrediente`) VALUES
(1, 1, 1),
(2, 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `local_nutriologos`
--

CREATE TABLE `local_nutriologos` (
  `Id_nutriologo` int(11) NOT NULL,
  `nom_nutriologo` varchar(30) DEFAULT NULL,
  `direccion` varchar(40) DEFAULT NULL,
  `num_tel` varchar(11) DEFAULT NULL,
  `imagen` varchar(255) NOT NULL,
  `fecha_created` datetime NOT NULL,
  `fecha_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `local_nutriologos`
--

INSERT INTO `local_nutriologos` (`Id_nutriologo`, `nom_nutriologo`, `direccion`, `num_tel`, `imagen`, `fecha_created`, `fecha_updated`) VALUES
(1, 'Jose', 'calle 46 entre 83 y 85, Plan de ayala', '9831310912', 'logopropuesta.png', '2019-04-09 18:00:41', '2019-06-12 03:07:50');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modo_preparacion`
--

CREATE TABLE `modo_preparacion` (
  `Id_modo` int(11) NOT NULL,
  `Id_platillo` int(11) NOT NULL,
  `pasos` varchar(255) NOT NULL,
  `descripcion` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `otra_ingre`
--

CREATE TABLE `otra_ingre` (
  `Id_ingre_otro` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `beneficios` varchar(255) NOT NULL,
  `cantidad` int(255) NOT NULL,
  `unidad_medida` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `padecidad`
--

CREATE TABLE `padecidad` (
  `Id_padecidad` int(11) NOT NULL,
  `nom_padecimiento` varchar(20) DEFAULT NULL,
  `descripcion` varchar(30) DEFAULT NULL,
  `fecha_created` datetime NOT NULL,
  `fecha_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `padecidad_analisis`
--

CREATE TABLE `padecidad_analisis` (
  `Id_padecidad` int(11) NOT NULL,
  `Id_analisis` int(11) NOT NULL,
  `fecha_created` datetime NOT NULL,
  `fecha_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `planta`
--

CREATE TABLE `planta` (
  `Id_planta` int(11) NOT NULL,
  `nom_planta` varchar(15) DEFAULT NULL,
  `descripcion_p` varchar(255) DEFAULT NULL,
  `beneficios_p` varchar(255) DEFAULT NULL,
  `imagen` varchar(255) NOT NULL,
  `fecha_created` datetime NOT NULL,
  `fecha_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `planta`
--

INSERT INTO `planta` (`Id_planta`, `nom_planta`, `descripcion_p`, `beneficios_p`, `imagen`, `fecha_created`, `fecha_updated`) VALUES
(1, 'Hepazote', 'Es una planta que tiene hojas la cual es la que se utiliza.', 'Tiene nutrientes naturales', 'logopropuesta.png', '2019-04-09 18:00:41', '2019-06-18 15:46:09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `platillo`
--

CREATE TABLE `platillo` (
  `Id_platillo` int(11) NOT NULL,
  `nom_platillo` varchar(255) DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `beneficios` varchar(255) DEFAULT NULL,
  `fecha_created` datetime NOT NULL,
  `fecha_updated` datetime NOT NULL,
  `imagen` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `platillo`
--

INSERT INTO `platillo` (`Id_platillo`, `nom_platillo`, `descripcion`, `beneficios`, `fecha_created`, `fecha_updated`, `imagen`) VALUES
(1, 'Cocktel de frutas', 'Es facil de preparar', 'Contiene vitaminas A,B', '2019-04-09 18:00:41', '2019-06-18 16:36:39', 'logopropuesta.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registro_padecidad`
--

CREATE TABLE `registro_padecidad` (
  `Id_padecidad` int(11) NOT NULL,
  `Id_user` int(11) NOT NULL,
  `fecha_created` datetime NOT NULL,
  `fecha_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registro_user`
--

CREATE TABLE `registro_user` (
  `Id_user` int(11) NOT NULL,
  `nombre` varchar(30) DEFAULT NULL,
  `correo` varchar(30) DEFAULT NULL,
  `contraseña` varchar(8) DEFAULT NULL,
  `edad` int(2) DEFAULT NULL,
  `peso` decimal(3,2) DEFAULT NULL,
  `altura` decimal(2,2) DEFAULT NULL,
  `fecha_created` datetime NOT NULL,
  `fecha_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `verdura`
--

CREATE TABLE `verdura` (
  `Id_verdura` int(11) NOT NULL,
  `nom_verdura` varchar(15) DEFAULT NULL,
  `descripcion_v` varchar(255) DEFAULT NULL,
  `beneficios_v` varchar(255) DEFAULT NULL,
  `imagen` varchar(255) NOT NULL,
  `fecha_created` datetime NOT NULL,
  `fecha_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `verdura`
--

INSERT INTO `verdura` (`Id_verdura`, `nom_verdura`, `descripcion_v`, `beneficios_v`, `imagen`, `fecha_created`, `fecha_updated`) VALUES
(1, 'Zanahoria', 'sin definir', 'En proceso de investigacion', 'logopropuesta.png', '2019-04-09 18:00:41', '2019-06-18 15:40:34');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `visitantes`
--

CREATE TABLE `visitantes` (
  `mes` varchar(255) NOT NULL,
  `cantidad_visitantes` int(11) NOT NULL,
  `fecha_visita` int(11) NOT NULL,
  `fecha_created` datetime NOT NULL,
  `fecha_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`Id_admin`);

--
-- Indices de la tabla `analisis_quimicos`
--
ALTER TABLE `analisis_quimicos`
  ADD PRIMARY KEY (`Id_analisis`);

--
-- Indices de la tabla `fruta`
--
ALTER TABLE `fruta`
  ADD PRIMARY KEY (`Id_fruta`);

--
-- Indices de la tabla `informacion_nutri`
--
ALTER TABLE `informacion_nutri`
  ADD PRIMARY KEY (`Id_informacion`);

--
-- Indices de la tabla `info_fruta`
--
ALTER TABLE `info_fruta`
  ADD PRIMARY KEY (`Id_info_fruta`),
  ADD KEY `info_fruta_ibfk_1` (`Id_fruta`),
  ADD KEY `info_fruta_ibfk_2` (`Id_informacion`);

--
-- Indices de la tabla `info_planta`
--
ALTER TABLE `info_planta`
  ADD PRIMARY KEY (`Id_info_planta`),
  ADD KEY `info_planta_ibfk_1` (`Id_planta`),
  ADD KEY `info_planta_ibfk_2` (`Id_informacion`);

--
-- Indices de la tabla `info_platillo`
--
ALTER TABLE `info_platillo`
  ADD PRIMARY KEY (`Id_info_platillo`),
  ADD KEY `info_platillo_ibfk_1` (`Id_platillo`),
  ADD KEY `info_platillo_ibfk_2` (`Id_informacion`);

--
-- Indices de la tabla `info_verdura`
--
ALTER TABLE `info_verdura`
  ADD PRIMARY KEY (`Id_info_verdura`),
  ADD KEY `info_verdura_ibfk_1` (`Id_verdura`),
  ADD KEY `info_verdura_ibfk_2` (`Id_informacion`);

--
-- Indices de la tabla `ingredientes`
--
ALTER TABLE `ingredientes`
  ADD PRIMARY KEY (`Id_ingrediente`),
  ADD KEY `ingrediente_ibfk_1` (`Id_ingrediente`);

--
-- Indices de la tabla `ingre_platillo`
--
ALTER TABLE `ingre_platillo`
  ADD PRIMARY KEY (`Id_ingre_platillo`),
  ADD KEY `ingre_platillo_ibfk_1` (`Id_platillo`),
  ADD KEY `ingre_platillo_ibfk_2` (`Id_ingrediente`);

--
-- Indices de la tabla `local_nutriologos`
--
ALTER TABLE `local_nutriologos`
  ADD PRIMARY KEY (`Id_nutriologo`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `padecidad`
--
ALTER TABLE `padecidad`
  ADD PRIMARY KEY (`Id_padecidad`);

--
-- Indices de la tabla `padecidad_analisis`
--
ALTER TABLE `padecidad_analisis`
  ADD KEY `padecidad_ibfk_1` (`Id_analisis`),
  ADD KEY `padecidad_ibfk_2` (`Id_padecidad`);

--
-- Indices de la tabla `planta`
--
ALTER TABLE `planta`
  ADD PRIMARY KEY (`Id_planta`);

--
-- Indices de la tabla `platillo`
--
ALTER TABLE `platillo`
  ADD PRIMARY KEY (`Id_platillo`);

--
-- Indices de la tabla `registro_padecidad`
--
ALTER TABLE `registro_padecidad`
  ADD PRIMARY KEY (`Id_padecidad`,`Id_user`),
  ADD KEY `registro_padecidad_ibfk_2` (`Id_user`);

--
-- Indices de la tabla `registro_user`
--
ALTER TABLE `registro_user`
  ADD PRIMARY KEY (`Id_user`);

--
-- Indices de la tabla `verdura`
--
ALTER TABLE `verdura`
  ADD PRIMARY KEY (`Id_verdura`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `administrador`
--
ALTER TABLE `administrador`
  MODIFY `Id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `analisis_quimicos`
--
ALTER TABLE `analisis_quimicos`
  MODIFY `Id_analisis` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `fruta`
--
ALTER TABLE `fruta`
  MODIFY `Id_fruta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `informacion_nutri`
--
ALTER TABLE `informacion_nutri`
  MODIFY `Id_informacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT de la tabla `info_fruta`
--
ALTER TABLE `info_fruta`
  MODIFY `Id_info_fruta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `info_planta`
--
ALTER TABLE `info_planta`
  MODIFY `Id_info_planta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `info_platillo`
--
ALTER TABLE `info_platillo`
  MODIFY `Id_info_platillo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `info_verdura`
--
ALTER TABLE `info_verdura`
  MODIFY `Id_info_verdura` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `ingredientes`
--
ALTER TABLE `ingredientes`
  MODIFY `Id_ingrediente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `ingre_platillo`
--
ALTER TABLE `ingre_platillo`
  MODIFY `Id_ingre_platillo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `local_nutriologos`
--
ALTER TABLE `local_nutriologos`
  MODIFY `Id_nutriologo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `padecidad`
--
ALTER TABLE `padecidad`
  MODIFY `Id_padecidad` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `planta`
--
ALTER TABLE `planta`
  MODIFY `Id_planta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `platillo`
--
ALTER TABLE `platillo`
  MODIFY `Id_platillo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `registro_user`
--
ALTER TABLE `registro_user`
  MODIFY `Id_user` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `verdura`
--
ALTER TABLE `verdura`
  MODIFY `Id_verdura` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `info_fruta`
--
ALTER TABLE `info_fruta`
  ADD CONSTRAINT `info_fruta_ibfk_1` FOREIGN KEY (`Id_fruta`) REFERENCES `fruta` (`Id_fruta`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `info_fruta_ibfk_2` FOREIGN KEY (`Id_informacion`) REFERENCES `informacion_nutri` (`Id_informacion`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `info_planta`
--
ALTER TABLE `info_planta`
  ADD CONSTRAINT `info_planta_ibfk_1` FOREIGN KEY (`Id_planta`) REFERENCES `planta` (`Id_planta`),
  ADD CONSTRAINT `info_planta_ibfk_2` FOREIGN KEY (`Id_informacion`) REFERENCES `informacion_nutri` (`Id_informacion`);

--
-- Filtros para la tabla `info_platillo`
--
ALTER TABLE `info_platillo`
  ADD CONSTRAINT `info_platillo_ibfk_1` FOREIGN KEY (`Id_platillo`) REFERENCES `platillo` (`Id_platillo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `info_platillo_ibfk_2` FOREIGN KEY (`Id_informacion`) REFERENCES `informacion_nutri` (`Id_informacion`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `info_verdura`
--
ALTER TABLE `info_verdura`
  ADD CONSTRAINT `info_verdura_ibfk_1` FOREIGN KEY (`Id_verdura`) REFERENCES `verdura` (`Id_verdura`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `info_verdura_ibfk_2` FOREIGN KEY (`Id_informacion`) REFERENCES `informacion_nutri` (`Id_informacion`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `ingre_platillo`
--
ALTER TABLE `ingre_platillo`
  ADD CONSTRAINT `ingre_platillo_ibfk_1` FOREIGN KEY (`Id_platillo`) REFERENCES `platillo` (`Id_platillo`),
  ADD CONSTRAINT `ingre_platillo_ibfk_2` FOREIGN KEY (`Id_ingrediente`) REFERENCES `ingredientes` (`Id_ingrediente`);

--
-- Filtros para la tabla `padecidad_analisis`
--
ALTER TABLE `padecidad_analisis`
  ADD CONSTRAINT `padecidad_ibfk_1` FOREIGN KEY (`Id_analisis`) REFERENCES `analisis_quimicos` (`Id_analisis`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `padecidad_ibfk_2` FOREIGN KEY (`Id_padecidad`) REFERENCES `padecidad` (`Id_padecidad`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `registro_padecidad`
--
ALTER TABLE `registro_padecidad`
  ADD CONSTRAINT `registro_padecidad_ibfk_1` FOREIGN KEY (`Id_padecidad`) REFERENCES `padecidad` (`Id_padecidad`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `registro_padecidad_ibfk_2` FOREIGN KEY (`Id_user`) REFERENCES `registro_user` (`Id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `registro_user`
--
ALTER TABLE `registro_user`
  ADD CONSTRAINT `registro_user_ibfk_1` FOREIGN KEY (`Id_user`) REFERENCES `analisis_quimicos` (`Id_analisis`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
