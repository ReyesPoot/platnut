<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Verdura extends Model 
{

protected $fillable= ['imagen','id_verdura','nom_verdura','descripcion_v','beneficios_v','fecha_created','fecha_updated'];

protected $primaryKey= 'Id_verdura';
protected $table= 'verdura';
const CREATED_AT ='fecha_created';
const UPDATED_AT ='fecha_updated';

public function informacion(){
		return $this->belongsToMany('App\Informacion', 'info_verdura', 'Id_verdura','Id_informacion');
	}
public function platillo(){
		return $this->belongsToMany('App\platillo', 'platillo_verdura', 'Id_verdura','Id_platillo');
	}
}

 ?>