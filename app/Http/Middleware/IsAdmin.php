<?php

namespace App\Http\Middleware;

use Closure;
Use Illuminate\Support\Facades\Auth;

Class IsAdmin{

public function handle($request, Closure $next)
{

	if(Auth::Guard('admin')->check()){
		return $next($request);
	} else {
		return redirect('/acceso');
	}
}

}
