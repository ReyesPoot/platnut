@extends('admin/layout/admin')
@section('content')
<!-- contenido de la pagina web -->
<div class="container" >
    <div class="cold-md-6">
        <h2>Eliminar Ingrediente </h2>
            <form action="/admin/EliminarIngrediente" method="POST">

                {{csrf_field()}}
                                
                                <input type="text" value="{{$ingrediente->Id_ingrediente}}" name="id" hidden>     
                            
                                <div class="form-group">
                                    <label for="agregar verduras">Ingrediente</label>
                                    <input type="text" value="{{$ingredientes->ingrediente}}" name="ingrediente" class= "form-control placeholder" placeholder="Nombre..">
                                </div>
                                <div class="form-group">
                                    <label for="agregar">Cantidad</label>
                                   <input type="text" value="{{$ingredientes->cantidad}}" name="cantidad" class= "form-control placeholder" placeholder="cantidad...">
                                </div>
                                <div class="form-group">
                                    <label for="agregar ">Unidad de medida</label>
                                    <input type="text" value="{{$ingredientes->unidad_medida}}" name="unidad_medida" class= "form-control placeholder" placeholder="">
                                </div>
                                
                <div class="form-group">
                    <button type="submit" class= "btn btn-outline-danger btn-block">Eliminar</button>
                    <!-- <button type="submit" class= "btn btn-danger">Eliminar</button> -->
                </div>
         </form> 
    </div>    
</div> 
       
@endsection
@section('js')

@endsection