<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class instrucciones_platillo extends Model 
{

	protected $fillable= ['Id_instru_platillo','instrucciones'];

	protected $primaryKey= 'Id_instru_platillo';
	protected $table= 'instrucciones_platillo';
	

	public function instrucciones_platillo(){
		return $this->belongsToMany('App\instrucciones_platillo', 'Id_instrucciones','Id_platillo');
	}
}

 ?>