@extends('admin/layout/admin')
@section('content')
 @php $urlImagen= 'images/'. $plantas->imagen; @endphp
    
<div class="container" >
  <div class="card mb-4">
    <div class="row">
      <div class="col-md-4">
          <img src="{{ asset($urlImagen)}}" class="img-fluid" alt="{{ asset($urlImagen)}}">
      </div>
      <div class="col-md-8">
          <div class="card-body">
            <h5 class="card-title">Planta:</h5>
            <p>{{$plantas->nom_planta}}</p>
            <h5 class="card-title">Descripcion</h5>
            <p>{{$plantas->descripcion_p}}</p>
          </div>
      </div>
    </div>

  <div class= "row">
      <div class="card-body">
        <div class= "col-md-5">
          <h5 class="card-title">Beneficio:</h5>
            <p class="card-text">{{$plantas->beneficios_p}}</p>
        </div>
      </div>
  </div>
  <div class="row">
      <div class="col-md-8">
        <div class= "card-body">
            <h5 class="card-title">Valor nutrimental:</h5> 

              <table class="table table-bordered">
              <thead class="thead-dark">
                
                <tr>
                  <th>Componente</th>
                  <th>Cantidad</th>
                  <th>Unidad</th>
                  <th>Acciones</th>
                  
                </tr>
              </thead>
              <tbody>
                @foreach($plantas->informacion as $informacion)
                <tr>
                  <td>{{$informacion->informacion}}</td>
                  <td>{{$informacion->cantidad}}</td>
                  <td>{{$informacion->unidad_medida}}</td>
                  <td> 

                     <a href="/admin/EliminarInfoPlanta/{{$informacion->Id_informacion}}/{{$plantas->Id_planta}}" class="btn btn-block btn-danger btn -xs">Eliminar</a>

                  </td>
                </tr>
                 @endforeach
              </tbody>
          </table>
        </div>
      </div>

      <!-- <div class="col-md-4">
        <div class="card-body">
          <h5 class="card-title">Receta:</h5>
          
        </div>
      </div> -->
    </div>
    <center>
      <a href="/admin/verPlantas" class="btn btn-outline-info">Regresar</a>
      <a href="/admin/EditarPlanta/{{$plantas->Id_planta}}" class="btn btn-success">Actualizar</a>
    </center>
  </div>
</div> 
@endsection
@section('js')
<!-- archivos js dependientes de la vista -->
@endsection
