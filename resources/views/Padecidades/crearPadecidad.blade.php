@extends('admin/layout/admin')
@section('content')
<!-- contenido de la pagina web -->
<div class="container" >
        <div class="cold-md-4">
            <h2>Agregar los tipos de padecidades</h2>
            <form action="/admin/agregarPadecidades" method="POST">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="agregar">Nombre del padecimiento</label>
                    <input type="text" name="padecidad" class= "form-control placeholder" placeholder="Nombre de la padecidad">
                </div>
                <div class="form-group">
                    <label for="agregar">Descripcion </label>
                    <textarea name="descripcion" class="form-control" rows="3" placeholder="escriba la descripcion de la padecidad"></textarea>
                </div>
                <div class="form-group">
                    <input type="submit" value="Guardar" class= "btn btn-success btn -xs">
                </div>
                 

             </form> 
         </div>    
</div>        
@endsection
@section('js')
<!-- archivos js dependientes de la vista -->
@endsection

       

