@extends('admin/layout/admin')
@section('content')
<!-- contenido de la pagina web -->
<div class="container">
  <div class="row">
    @foreach ($nutriologos as $local_nutriologo)
    @php $urlImagen= 'images/'. $local_nutriologo->imagen; @endphp
      
    <div class="col-md-3 my-3">
       <div class="card">
        <img src="{{ asset($urlImagen)}}" class="card-img-top" alt="{{ asset($urlImagen)}}">
          <div class="card-body">
            <h5 class="card-title">Dirreccion</h5>
            <p><th>{{ $local_nutriologo->direccion}}</th></p>
            <a href="/admin/EditarNutriologo/{{$local_nutriologo->Id_nutriologo}}" class="btn btn-block btn-info">Actualizar</a>
            <a href="/admin/EliminarNutriologo/{{$local_nutriologo->Id_nutriologo}}" class="btn btn-block btn-danger">Eliminar</a>
              <a href="/admin/Consultar/{{$local_nutriologo->Id_nutriologo}}" class="btn btn-block btn-success">ver</a>
          </div>
      </div>
    </div> 

    @endforeach
  </div>
</div> 
     
          
@endsection
@section('js')
<!-- archivos js dependientes de la vista -->
@endsection