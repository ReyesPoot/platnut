<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingrediente extends Model 
{

protected $fillable= ['Id_ingrediente','ingrediente','cantidad','unidad_medida','imagen','fecha_created','fecha_updated'];

protected $primaryKey= 'Id_ingrediente';
protected $table= 'ingredientes';
const CREATED_AT ='fecha_created';
const UPDATED_AT ='fecha_updated';

// relacion N:N

public function platillo(){
		return $this->belongsToMany('App\Platillo', 'ingre_platillo', 'Id_ingrediente','Id_platillo');
	}


}


 ?>