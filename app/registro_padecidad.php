<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class registro_padecidad extends Model 
{

protected $fillable= ['Id_padecidad','Id_user','fecha_created','fecha_updated'];

//protected $primaryKey= 'Id_analisis';
protected $table= 'registro_padecidad';
const CREATED_AT ='fecha_created';
const UPDATED_AT ='fecha_updated';

//Relacionens 

public function registro_padecidad(){
	return $this->belongsToMany('App/Padecidad','Id_padecidad','Id_padecidad');

}
public function informacion(){
	return $this->belongsToMany('App/registro_user','Id_user','Id_user');

}

}

 ?>