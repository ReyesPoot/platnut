/*---------------------------------
   sidebar collapse in admin platilla
 ----------------------------------*/

(function () {
	"use strict";

	var treeviewMenu = $('.app-menu');

	// Toggle Sidebar
	$('[data-toggle="sidebar"]').click(function(event) {
		event.preventDefault();
		$('.app').toggleClass('sidenav-toggled');
	});

	// Activate sidebar treeview toggle
	$("[data-toggle='treeview']").click(function(event) {
		event.preventDefault();
		if(!$(this).parent().hasClass('is-expanded')) {
			treeviewMenu.find("[data-toggle='treeview']").parent().removeClass('is-expanded');
		}
		$(this).parent().toggleClass('is-expanded');
	});

	// Set initial active toggle
	$("[data-toggle='treeview.'].is-expanded").parent().toggleClass('is-expanded');

})();
/*---------------------------------------------------------
   inicialización del complemento d bootstrap de file input
 ----------------------------------------------------------*/
 
$("#file-1").fileinput({
    theme: 'fas',
    uploadUrl: '#', // you must set a valid URL here else you will get an error
    allowedFileExtensions: ['docx', 'doc', 'pdf'],
    overwriteInitial: false,
    maxFileSize: 3072,
    maxFilesNum: 1,
    //allowedFileTypes: ['image', 'video', 'flash'],
    slugCallback: function (filename) {
        return filename.replace('(', '_').replace(']', '_');
    }
});
$("#file-2").fileinput({
    theme: 'fas',
    uploadUrl: '#', // you must set a valid URL here else you will get an error
    allowedFileExtensions: ['png', 'jpg', 'jpeg'],
    overwriteInitial: false,
    maxFileSize: 1650,
    maxFilesNum: 1,
    maxImageWidth: 768,
    maxImageHeight: 522,
    //allowedFileTypes: ['image', 'video', 'flash'],
    slugCallback: function (filename) {
        return filename.replace('(', '_').replace(']', '_');        
    }    
});

/*---------------------------------------------------------
   inicialización del complemento d bootstrap datepicker
 ----------------------------------------------------------*/
 $('.demoDate').datepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    todayHighlight: true

});