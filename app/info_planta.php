<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class info_planta extends Model 
{

protected $fillable= ['Id_planta','Id_informacion','fecha_created','fecha_updated'];

//protected $primaryKey= '';
protected $table= 'info_planta';
const CREATED_AT ='fecha_created';
const UPDATED_AT ='fecha_updated';

//Relaciones

public function info_planta(){
	return $this->belongsToMany('App/Planta','Id_planta','Id_planta');

}
public function informacion(){
	return $this->belongsToMany('App/Informacion','Id_informacion','Id_informacion');

}

}

 ?>