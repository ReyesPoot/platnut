@extends('admin/layout/admin')
@section('content')
<!-- contenido de la pagina web -->
<div class="container" >
    <div class="cold-md-6">
        <h2>Listado de los tipos de padecidades mas comunes</h2>
        <div>
          <div class= "panel panel-default">
          </div>
           <table class= "table table_responsive table-bordered">
               <thead>

                    <th>ID</th>
                    <th>Padecimiento</th>
                    <th>Descripcion</th>
                    <th>Acciones</th>
               </thead>
               <tbody>

                @foreach ($padecidades as $padecidad)
                    <tr>
                    <th>{{ $padecidad->Id_padecidad}} </th>
                    <th>{{ $padecidad->nom_padecimiento}}</th>
                    <th>{{ $padecidad->descripcion}}</th>
                     <th>
                       <a href="/admin/EditarPadecidad/{{$padecidad->Id_padecidad}}" class="btn btn-warning btn -xs" >Editar</a>
                       <a href="/admin/EliminarPadecidad/{{$padecidad->Id_padecidad}}" class="btn btn-danger btn -xs" >Eliminar</a>


                     </th>
                    </tr>  

                @endforeach 

               </tbody>
           </table>
    
        </div>
    </div>    
</div> 
       
@endsection
@section('js')
<!-- archivos js dependientes de la vista -->
@endsection