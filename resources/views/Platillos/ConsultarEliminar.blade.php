@extends('admin/layout/admin')
@section('content')
<!-- contenido de la pagina web -->

    @php $urlImagen= 'images/'. $platillo->imagen; @endphp
    
<div class="container">
  <div class="card mb-3">
    <div class="row ">
      <div class="col-md-4">
        <img src="{{ asset($urlImagen)}}" class="img-fluid" alt="{{ asset($urlImagen)}}">
      </div>
      <div class="col-md-8">
        <div class="card-body">
          <h5 class="card-title">PLATILLO</h5>
            <p class="card-text">{{$platillo->nom_platillo}}</p>
          <h5 class="card-title">DESCRIPCION</h5>
          <p class="card-text">{{$platillo->descripcion}}</p>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-5">
        <div class="card-body">
          <h5 class="card-title">BENEFICIOS</h5>
            <p class="card-text">{{$platillo->beneficios}}</p>
        </div>
      </div>
      <div class="col-md-7">
        <div class="card-body">
          <h5 class="card-title">VALOR NUTRICIONAL</h5>
          
            <table class="table table-bordered">
              <thead class="thead-dark">
                <tr>
                  <th>Componentes</th>
                  <th>Cantidad</th>
                  <th>Unidad_medida</th>
                  <th>Acciones</th>
                  
               
                </tr>
                </thead>
                <tbody>
                  @foreach($platillo->informacion as $informacion)
                  <tr>
                    <td>{{$informacion->informacion}}</td>
                    <td>{{$informacion->cantidad}}</td>
                    <td>{{$informacion->unidad_medida}}</td>
                    <td>
                       <a href="/admin/EliminarInfoPlatillo/{{$informacion->Id_informacion}}/{{$platillo->Id_platillo}}" class="btn btn-block btn-danger btn -xs">Eliminar</a>

                    </td>             
                  </tr>
                  @endforeach
                </tbody>
            </table>
        </div>
      </div>
    </div>
    <div class="row">
       <div class="col-md-6">
          <div class="card-body">
            <h5 class="card-title">INGREDIENTES: </h5>
              @foreach($platillo->ingredientes as $ingredientes) 
             <p>{{$ingredientes->cantidad}} {{$ingredientes->unidad_medida}} de {{$ingredientes->ingrediente}}
               <a href="/admin/EliminarIngrediente/{{$ingredientes->Id_ingrediente}}/{{$platillo->Id_platillo}}" class=" btn-danger btn -xs">Eliminar</a>
            </p>
              @endforeach
          </div>   
        </div>
          <div class="col-md-6">
            <div class="card-body">
              <h5 class="card-title">PREPARACION</h5>
                <?php $c=1;  ?>
                    @foreach($platillo->instrucciones as $instrucciones)
                        <p> paso {{$c++}}:{{$instrucciones->instrucciones}} 
                          <a href="/admin/EliminarInstrucciones/{{$instrucciones->Id_instrucciones}}/{{$platillo->Id_platillo}}" class="btn btn-danger btn -xs">Eliminar</a>
                        </p>
                     @endforeach     
            </div>
          </div>
          </div>
    <center>
      <a href="/admin/verPlatillo" class="btn btn-outline-info">Regresar</a>
      <a href="/admin/ConsultarActualizar/{{$platillo->Id_platillo}}" class="btn btn-success">Actualizar</a>
    </center> 
      </div>
  </div>   
</div> 
@endsection
@section('js')
<!-- archivos js dependientes de la vista -->
@endsection
