<?php

namespace App\Http\Controllers;

use App\Analisis;
use Illuminate\Http\Request; 

use App\Http\Controllers\Controller;

class AnalisisController extends Controller
{

    
    public function agregarAnalisis()
    {
        return view ('Analisis/crearAnalisis');
    }
     public function store(Request $request)
    {

        $Guardar= new Analisis;

        $Guardar->nom_analisis= $request->analisis;
        $Guardar->tipo= $request->tipo;
        //dd($Guardar);
        $Guardar->save();
        return redirect('/admin/verAnalisis'); 
    }

    public function verAnalisis()
    {
        $analisis= Analisis::get();
        return view ('Analisis/verAnalisis', compact('analisis'));
    }
    public function EditarAnalisis($id)
    {
    
        $analisis= Analisis::where('Id_analisis',$id)->first();
        return view ('Analisis/actualizarAnalisis', compact('analisis'));

    }
    public function updateAnalisis(Request $request)
    {   //dd($_POST);
        
        $analisis= Analisis::where('Id_analisis',$request->id)->first();
        $analisis->nom_analisis=$request->analisis;
        $analisis->tipo=$request->tipo;
        $analisis->save();
        
        return redirect('/admin/verFruta'); 

    }
    public function EliminarAnalisis($id)
    {
        //dd($id);
        $analisis= Analisis::where('Id_analisis',$id)->first();
         return view ('Analisis/EliminarAnalisis', compact('analisis'));

    } 
    
    public function destroy(Request $request)
    {   
        
        $analisis= Analisis::where('Id_analisis',$request->id)->first();
        $analisis->delete();
        return redirect('/admin/verAnalisis');
        
    }

}