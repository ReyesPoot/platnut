@extends('admin/layout/admin')
@section('content')
<!-- contenido de la pagina web -->
<div class="container">
  <div class="row">
    @foreach ($platillos as $platillo)
    
    @php $urlImagen= 'images/'. $platillo->imagen; @endphp
      
    <div class="col-md-3 my-4">
       <div class="card">
        <img src="{{ asset($urlImagen)}}" class="card-img-top" alt="{{ asset($urlImagen)}}">
          <div class="card-body">
            <h5 class="card-title">Platillo</h5>
            <p><th>{{ $platillo->nom_platillo }}</th></p>
            <h5 class="card-title">Descripcion</h5>
            <p><th>{{ $platillo->descripcion }}</th></p>
            
            <!-- Botones CRUD -->
            <a href="/admin/ConsultarPlatillos/{{$platillo->Id_platillo}}" class="btn btn-block btn-primary">ver</a>
            
          </div>
      </div>
    </div> 
    @endforeach
  </div>
</div> 
     
@endsection
@section('js')
<!-- archivos js dependientes de la vista -->
@endsection