@extends('admin/layout/admin')
@section('content')
<!-- contenido de la pagina web -->
<div class="container" >
    <div class="cold-md-6">
        <h2>Eliminar fruta</h2>
            <form action="/admin/EliminarAnalisis" method="POST">
                {{csrf_field()}}

                <input type="text" value="{{$analisis->Id_analisis}}" name="id" hidden>
                <div class="form-group">
                    <label for="agregar frutas">Nombre del analisis</label>
                    <input type="text" value="{{$analisis->nom_analisis}}" name="analisis" class= "form-control placeholder" placeholder="Nombre del analisis">
                </div>
                <div class="form-group">
                    <label for="agregar frutas">Tipo de analisis</label>
                    <textarea class="form-control" name="tipo" rows="3" placeholder="Tipo de analisis">{{$analisis->tipo}}</textarea>
                </div>
                <div class="form-group">
                    <button type="submit" class= "btn btn-success">Eliminar</button>
                </div>
                
                 
         </form> 
    </div>    
</div> 
       
@endsection
@section('js')
<!-- archivos js dependientes de la vista -->
@endsection
