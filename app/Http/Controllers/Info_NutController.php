<?php

namespace App\Http\Controllers;

use App\Informacion;
use App\Fruta;
use App\Verdura;
use App\Platillo;
use App\Planta;
use App\info_fruta;
use Illuminate\Http\Request; 

use App\Http\Controllers\Controller;

class Info_NutController extends Controller
{
  
    //se usara para listar informacion nutrimental y su respectiva planta, fruta, verdura y platillo
    //actualmente solo lista informacion nutrimental
    public function ver()
    {
        $title = "verInfo_Nutrimental";
        $informacion = Informacion::get();
        return view ('inf_Nutrimental/verInfo_Nutrimental', compact('informacion','title'));
    }

    //guardando info de fruta y su informacion nutrimental
    public function storeFruta(Request $request )
    {
        $Guardar= new informacion;
        
        $Guardar->informacion= $request->informacion;
        $Guardar->cantidad= $request->cantidad;
        $Guardar->unidad_medida= $request->unidad;
         // dd($Guardar);
        $Guardar->save();

         $id_info=$Guardar->Id_informacion;//obteniendo el id de la informacion
                                       //agregando informacion nutrimental de los prodcutos en la tabla pivote
        $info=Fruta::find($request->id);

        $info->informacion()->attach($id_info);

        return redirect('/admin/ConsultarFruta/'. $request->id); 
    }

    //guardando informacion nutrimental de verdura 
    public function storeVerdura(Request $request )
    {
        $Guardar= new informacion;
        $Guardar->informacion= $request->informacion;
        $Guardar->cantidad= $request->cantidad;
        $Guardar->unidad_medida= $request->unidad;
        $Guardar->save();

        $id_info=$Guardar->Id_informacion;//obteniendo el id de la informacion
                                       //agregando informacion nutrimental de los prodcutos en la tabla pivote
        $info=Verdura::find($request->id);
        $info->informacion()->attach($id_info);
        return redirect('/admin/ConsultarVerdura/'. $request->id); 
    }
     public function storePlanta(Request $request ){
        $Guardar= new informacion;
        $Guardar->informacion= $request->informacion;
        $Guardar->cantidad= $request->cantidad;
        $Guardar->unidad_medida= $request->unidad;
        $Guardar->save();

        $id_info=$Guardar->Id_informacion;//obteniendo el id de la informacion
                                       //agregando informacion nutrimental de los prodcutos en la tabla pivote
        $info=Planta::find($request->id);
        $info->informacion()->attach($id_info);
        return redirect('/admin/ConsultarPlanta/'. $request->id); 
    }

     public function storePlatillo(Request $request ){
     
        $Guardar= new informacion;
        
        $Guardar->informacion= $request->informacion;
        $Guardar->cantidad= $request->cantidad;
        $Guardar->unidad_medida= $request->unidad;
         
        $Guardar->save();

        $id_info=$Guardar->Id_informacion;//obteniendo el id de la informacion
                                       //agregando informacion nutrimental de los prodcutos en la tabla pivote
        $info=Platillo::find($request->id);
        $info->informacion()->attach($id_info);
        return redirect('/admin/ConsultarPlatillos/'.$request->id); 
    }

// Actualizando la informacion nutrimental de la fruta
    public function EditarInformacionFruta($id_info_fruta, $id){


        $informacion = Informacion::where('Id_Informacion', $id)->first();
        $id_fruta = $id_info_fruta;
        return view ('Inf_Nutrimental/ActualizarInfo_fruta', compact('informacion', 'id_fruta'));
    }


    public function updateInformacion(Request $request){   
        
        $informacion = Informacion::where('Id_informacion',$request->id)->first();

        $informacion->informacion=$request->informacion;
        $informacion->cantidad=$request->cantidad;
        $informacion->unidad_medida=$request->unidad;
        $informacion->save();
        return redirect("/admin/EditarFruta/$request->id_fruta"); 

    }


// -----------Actualizando la informacion nutrimental de las verduras-----------


    public function EditarInformacionVerdura($id_info_verdura, $id){


            $informacion = Informacion::where('Id_Informacion', $id)->first();
            $id_verdura = $id_info_verdura;
            return view ('Inf_Nutrimental/ActualizarInfo_verdura', compact('informacion', 'id_verdura'));
        }


        public function updateInformacionVerdura(Request $request){   
            
            $informacion = Informacion::where('Id_informacion',$request->id)->first();

            $informacion->informacion=$request->informacion;
            $informacion->cantidad=$request->cantidad;
            $informacion->unidad_medida=$request->unidad;
            $informacion->save();
            return redirect("/admin/EditarVerdura/$request->id_verdura"); 
        }


//---------- Actualizando la informacion nutrimental de las plantas-----------


        public function EditarInformacionPlanta($id_info_planta, $id){


            $informacion = Informacion::where('Id_Informacion', $id)->first();
            $id_planta = $id_info_planta;
            return view ('Inf_Nutrimental/ActualizarInfo_planta', compact('informacion', 'id_planta'));
        }


        public function updateInformacionPlanta(Request $request){   
            
            $informacion = Informacion::where('Id_informacion',$request->id)->first();

            $informacion->informacion=$request->informacion;
            $informacion->cantidad=$request->cantidad;
            $informacion->unidad_medida=$request->unidad;
            $informacion->save();
            return redirect("/admin/EditarPlanta/$request->id_planta"); 
        }

// -------------Actualizando informacion nutrimental de los platillos--------


        public function EditarInformacionPlatillo($id_info_platillo, $id){


            $informacion = Informacion::where('Id_Informacion', $id)->first();
            $id_platillo = $id_info_platillo;
            return view ('Inf_Nutrimental/ActualizarInfo_platillo', compact('informacion', 'id_platillo'));
        }


        public function updateInformacionPlatillo(Request $request){   
            
            $informacion = Informacion::where('Id_informacion',$request->id)->first();

            $informacion->informacion=$request->informacion;
            $informacion->cantidad=$request->cantidad;
            $informacion->unidad_medida=$request->unidad;
            $informacion->save();
            return redirect("/admin/ConsultarActualizar/$request->id_platillo"); 
        }


// Eliminando la informacion nutrimental de los productos------

        
        public function EliminarInfoFruta($id, $id_fruta){
            $informacion = Informacion::find($id);
            $informacion->delete();

            return redirect('/admin/EliminarFruta/' . $id_fruta);
        } 
        
        public function EliminarInfoVerdura($id, $id_verdura)
        { 
            
            $informacion = Informacion::find($id);
            $informacion->delete();

            return redirect('/admin/EliminarVerdura/'. $id_verdura);
            
        }
         public function EliminarInfoPlanta($id, $id_planta)
        { 
            
            $informacion = Informacion::find($id);
            $informacion->delete();

            return redirect('/admin/EliminarPlanta/'. $id_planta);
            
        }
        public function EliminarInfoPlatillos($id, $id_platillo)
        { 
            
            $informacion = Informacion::find($id);
            $informacion->delete();

            return redirect('admin/ConsultarEliminar/'. $id_platillo);
            
        }

}