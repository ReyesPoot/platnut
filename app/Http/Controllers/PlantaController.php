<?php

namespace App\Http\Controllers;

use App\Planta;
use App\Informacion;
use App\Platillo;
use App\Ingrediente;
use Illuminate\Http\Request; 

use App\Http\Controllers\Controller;

class PlantaController extends Controller
{

    public function agregar()
    {
        $title="crearPlanta";
        return view ('plantas/crearPlanta',compact('title'));
    }
//consultando informacion nuutrimental, platillos,plantas
    public function Consultar($id)
    {
        $title="crearPlanta";
        
        $platillo=Platillo::where('Id_platillo',$id)->get();
        $planta=Planta::where('Id_planta',$id)->first();
        $Consulta=Planta::find($id)->informacion()->get();
        $ingrediente=Ingrediente::where('Id_ingrediente',$id)->get();
        // dd($Consulta);
        return view('plantas/ConsultarPlanta',compact('title','Consulta','ingrediente','planta','platillo'));


    }
    public function ver()
    {
        $title="crearPlanta";
        $plantas= Planta::get();
        return view ('plantas/verPlanta', compact('plantas','title'));
    }

    public function store(Request $request)
    {

        $Guardar= new planta;
        $Guardar->imagen= $request->imagen;
        $Guardar->nom_planta= $request->planta;
        $Guardar->descripcion_p= $request->descripcion;
        $Guardar->beneficios_p= $request->beneficios;
       
        //dd($Guardar);
        $Guardar->save();
        return redirect('/admin/verPlantas'); 
    }



    public function EditarPlanta($id)
    {
        $title="crearPlanta";
        $planta= Planta::where('Id_planta',$id)->first();
        $platillo=Platillo::where('Id_platillo',$id)->first();
        return view ('plantas/actualizarPlanta', compact('title','planta','platillo'));

    }

    public function updatePlanta(Request $request)
    {   //dd($_POST);
        
        $planta= Planta::where('Id_planta',$request->id)->first();
        $planta->imagen=$request->imagen;
        $planta->nom_planta=$request->planta;
        $planta->descripcion_p=$request->descripcion;
        $planta->beneficios_p=$request->beneficios;
     

        $planta->save();
        
        return redirect('/admin/verPlantas'); 

    }
    public function EliminarPlanta($id)
    {
        $title="crearPlanta";
        //dd($id);
        $plantas= Planta::where('Id_planta',$id)->first();

         return view ('Plantas/eliminarPlanta', compact('title','plantas'));

    } 
    public function destroy(Request $request)
    {   
        
        $plantas= Planta::where('Id_planta',$request->id)->first();
        $plantas->delete();
        return redirect('/admin/verPlantas');
        
    }
}