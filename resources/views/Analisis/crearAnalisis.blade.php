@extends('admin/layout/admin')
@section('content')
<!-- contenido de la pagina web -->
<div class="container" >
    <div class="cold-md-6">
        <h2>Agregar Analisis</h2>
            <form action="/admin/agregarAnalisis" method="POST">

                {{csrf_field()}}

                <div class="form-group">
                    <label for="agregar frutas">Nombre del analisis</label>
                    <input type="text" name="analisis" class= "form-control placeholder" placeholder="Nombre del analisis">
                </div>

                <div class="form-group">
                    <label for="agregar frutas">Tipo </label>
                    <textarea class="form-control" name="tipo" rows="3" placeholder="Tipo de analisis"></textarea>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlFile1">Subir Analisis</label>
                    <input type="file" class="form-control-file" id="exampleFormControlFile1">
                </div>
                <div class="form-group">
                    <input type="submit" value="enviar" class= "btn btn-primary">
                </div>
                 
         </form> 
    </div>    
</div> 
       
@endsection
@section('js')
<!-- archivos js dependientes de la vista -->
@endsection

       

