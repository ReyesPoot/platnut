<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Platillo extends Model 
{

protected $fillable= ['imagen','Id_platillo','nom_platillo','descripcion','beneficios','fecha_created','fecha_updated'];

protected $primaryKey= 'Id_platillo';
protected $table= 'platillo';
const CREATED_AT ='fecha_created';
const UPDATED_AT ='fecha_updated';

public function informacion(){
		return $this->belongsToMany('App\Informacion', 'info_platillo', 'Id_platillo','Id_informacion');
	}
public function ingredientes(){
		return $this->belongsToMany('App\Ingrediente', 'ingre_platillo', 'Id_platillo','Id_ingrediente');
	}

public function platillo_fruta(){
		return $this->belongsToMany('App\fruta', 'platillo_fruta', 'Id_fruta','Id_platillo');
	}
	public function platillo_planta(){
		return $this->belongsToMany('App\platillo', 'platillo_planta', 'Id_planta','Id_planta');
	}
	public function verdura(){
		return $this->belongsToMany('App\verdura', 'platillo_verdura', 'Id_platillo','Id_verdura');
	}
	public function instrucciones(){
		return $this->belongsToMany('App\instrucciones', 'instrucciones_platillo', 'Id_platillo','Id_instrucciones');
	}

}

 ?>