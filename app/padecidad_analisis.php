<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class padecidad_analisis extends Model 
{

protected $fillable= ['Id_padecidad','Id_padecidads','fecha_created','fecha_updated'];

//protected $primaryKey= 'Id_analisis';
protected $table= 'padecidad_analisis';
const CREATED_AT ='fecha_created';
const UPDATED_AT ='fecha_updated';

//Relaciones 

public function analisis_padecidad(){
	return $this->belongsToMany('App/Padecidad','Id_padecidad','Id_padecidad');

}
public function user_analisis(){
	return $this->belongsToMany('App/registro_user','Id_user','Id_user');

}
}

 ?>